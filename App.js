import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import IconBadge from 'react-native-icon-badge';
import { Provider } from 'react-redux';
import EventEmitter from "react-native-eventemitter";
import {ScaledSheet} from "react-native-size-matters";

import SplashScreen from './main/screens/SplashScreen';
import LoginScreen from './main/screens/LoginScreen';
import DashboardScreen from './main/screens/DashboardScreen';
import LeadListScreen from './main/screens/LeadListScreen';
import AccountScreen from './main/screens/AccountScreen';
import ProfileScreen from './main/screens/ProfileScreen';
import CSSPTScreen from './main/screens/CSSPTScreen';
import BankDetailsScreen from './main/screens/BankDetailsScreen';
import MyReviewScreen from './main/screens/MyReviewScreen';
import ServicesScreen from './main/screens/ServicesScreen';
import SalaryDetailsScreen from './main/screens/SalaryDetailsScreen';
import TargetDetailsScreen from './main/screens/TargetDetailsScreen';
import LeadDetailsScreen from './main/screens/LeadDetailsScreen';
import InvoiceScreen from './main/screens/InvoiceScreen';

import Constants from './main/common/Constants';
import Store from './main/store/Store';
import TextView from "./main/utils/TextView";

const Tab = createMaterialBottomTabNavigator();
const Stack = createNativeStackNavigator();

const TabNavigator = () => {
    const [badgeCount, setBadgeCount] = useState({new: 0, ongoing: 0});
    EventEmitter.on('updateBadgeCount', (value) => {
      setBadgeCount(value);
    });

  return(
    <Tab.Navigator
      initialRouteName="Splash"
      activeColor="black"
      inactiveColor='gray'
      barStyle={{ backgroundColor: 'white' }}>
      <Tab.Screen 
        name={Constants.dashboard}
        component={DashboardScreen} 
        options={{
          tabBarIcon: ({ color }) => (
            <MIcon name="view-dashboard" color={color} size={localStyles.iconSize} />
          ),
        }}/>
      <Tab.Screen 
        name={Constants.newLeads}
        component={LeadListScreen} 
        initialParams={{ "newLead": true }}
        options={{
            tabBarIcon: ({tintColor}) =>
                <IconBadge
                    IconBadgeStyle={localStyles.iconBadgeStyle}
                    MainElement={<Icon name='ios-list-outline' size={localStyles.iconSize} color={tintColor} />}
                    BadgeElement={<TextView text={badgeCount?.new || 0} style={localStyles.badgeText}/>}
                    Hidden={(badgeCount?.new || 0) === 0}
                />
        }}/>
      <Tab.Screen 
        name={Constants.ongoingLeads}
        component={LeadListScreen} 
        initialParams={{ "newLead": false }}
        options={{
            tabBarIcon: ({tintColor}) =>
                <IconBadge
                    IconBadgeStyle={localStyles.iconBadgeStyle}
                    MainElement={<Icon name='ios-repeat-outline' size={localStyles.iconSize} color={tintColor} />}
                    BadgeElement={<TextView text={badgeCount?.ongoing || 0} style={localStyles.badgeText}/>}
                    Hidden={(badgeCount?.ongoing || 0) === 0}
                />
          // tabBarIcon: ({ color }) => (
          //   <Icon name="ios-repeat-outline" color={color} size={26} />
          // ),
          // tabBarBadge: badgeCount?.ongoing || 0,
        }}/>
      <Tab.Screen 
        name={Constants.account} 
        component={AccountScreen} 
        options={{
          tabBarIcon: ({ color }) => (
            <Icon name="person" color={color} size={localStyles.iconSize} />
          ),
        }}/>
    </Tab.Navigator>
  )
}

const App = () => {
  return (
    <Provider store={Store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Splash">
          <Stack.Screen
              name="Splash"
              component={SplashScreen}
              options={{headerShown: false}}/>
          <Stack.Screen
              name="Login"
              component={LoginScreen}
              options={{headerShown: false}}/>
          <Stack.Screen
              name="Tabs"
              component={TabNavigator}
              options={{headerShown: false}}/>
          <Stack.Screen
              name="Profile"
              component={ProfileScreen}
              options={{headerShown: false}}/>
          <Stack.Screen
              name="CSSPT"
              component={CSSPTScreen}
              options={{headerShown: false}}/>
          <Stack.Screen
              name="BankDetails"
              component={BankDetailsScreen}
              options={{headerShown: false}}/>
          <Stack.Screen
              name="MyReviews"
              component={MyReviewScreen}
              options={{headerShown: false}}/>
          <Stack.Screen
              name="Services"
              component={ServicesScreen}
              options={{headerShown: false}}/>
          <Stack.Screen
              name="SalaryDetails"
              component={SalaryDetailsScreen}
              options={{headerShown: false}}/>
          <Stack.Screen
              name="TargetDetails"
              component={TargetDetailsScreen}
              options={{headerShown: false}}/>
          <Stack.Screen
              name="LeadDetails"
              component={LeadDetailsScreen}
              options={{headerShown: false}}/>
          <Stack.Screen
              name="Invoice"
              component={InvoiceScreen}
              options={{headerShown: false}}/>
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
};
const localStyles = ScaledSheet.create({
    iconSize: "26@ms",
    iconBadgeStyle: {minWidth: "17@ms", height: "17@ms", right: 0, left: "15@ms", top: "-2@ms"},
    badgeText: { fontSize: "10@ms", color: 'white', textAlign: 'center'}
})

export default App;
