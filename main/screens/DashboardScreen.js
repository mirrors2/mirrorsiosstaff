import React, {useEffect, useState} from 'react';
import {ScrollView, View, DeviceEventEmitter} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScaledSheet } from 'react-native-size-matters';
import {useDispatch, useSelector } from 'react-redux'
import EventEmitter from "react-native-eventemitter";

import {save} from '../utils/SharedPreferences';
import PreferenceKeys from '../common/PreferenceKeys';
import {dashboardDataRequest} from '../api/HandleRequest';

import Progress from '../components/Progress';
import HeaderComponent from '../components/HeaderComponent';
import color from '../resources/color';
import DashboardCard from '../components/DashboardCard';

import UserModal from '../modal/UserModal';
import Constants from '../common/Constants';
import Store from '../store/Store';
import { getUserData } from '../store/AppReducer';


const DashboardScreen = props => {

  const dispatch = useDispatch()
  const userData = new UserModal(useSelector(state => getUserData(state)));
  const [shouldUpdateData, setShouldUpdateData] = useState(false);
  const [dashboardData, setDashboardData] = useState();
  const [showProgress, setShowProgress] = useState(true);

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('tabPress', (e) => {
      const updateData = Store.getState().AppReducer.updateData
      if(updateData) setShouldUpdateData(updateData)
    });
    return unsubscribe;
  }, [props.navigation]);

  useEffect(() => {
    dashboardDataRequest({payload: {"staffid": userData.id}}).then(async (result) => {
      const body = result?.body;
      await save(PreferenceKeys.bankData, JSON.stringify(body?.bank_details));
      await save(PreferenceKeys.currency, JSON.stringify(body?.currency));
      await save(PreferenceKeys.count, JSON.stringify(body?.count));

      try{
        dispatch({ type: 'badgeCount', badgeCount: body.count })
        dispatch({type: 'bankData', bankData: body?.bank_details})
        dispatch({ type: 'currency', currency: body.currency || {currency: 'AED'}})
        dispatch({ type: 'updateData', updateData: false })
        EventEmitter.emit('updateBadgeCount', body.count)
      }catch(e){console.log('error', e)}

      setDashboardData(body?.dashboard);
      setShowProgress(false)

    }).catch(e => setShowProgress(false));
  }, [shouldUpdateData]);

  return (
    <View style={localStyles.container}>
      <SafeAreaView>
        <HeaderComponent 
          title={Constants.dashboard}
          description={Constants.dashboardDec}/>

          <ScrollView>
            <View style={localStyles.scrollContainer}>
              <View style={localStyles.cardContiner}>
                <DashboardCard 
                  count={dashboardData?.total || 0}
                  lable={'Total Leads'}
                  circleColor={"#eeeeee"}
                  iconName={'ios-list-outline'}
                  iconColor={"#545454"}/>

                <DashboardCard 
                  style={localStyles.cardRight}
                  count={dashboardData?.completed || 0}
                  lable={'Completed'}
                  circleColor={"#e9fbe6"}
                  iconName={'ios-checkmark-outline'}
                  iconColor={"#23D600"}/>
              </View>
              <View style={localStyles.cardContiner}>
                <DashboardCard 
                  count={dashboardData?.ongoing || 0}
                  lable={'Ongoing'}
                  circleColor={"#e7eaf1"}
                  iconName={'ios-repeat-outline'}
                  iconColor={"#112d6f"}/>

                <DashboardCard 
                  style={localStyles.cardRight}
                  count={dashboardData?.cancelled || 0}
                  lable={'Cancelled'}
                  circleColor={"#ffe6e6"}
                  iconName={'ios-close-outline'}
                  iconColor={"#Ff0000"}/>
              </View>
              <View style={localStyles.cardContiner}>
                <DashboardCard 
                  count={dashboardData?.rating || 0}
                  lable={'Rating'}
                  circleColor={"#fff6e6"}
                  iconName={'ios-star-outline'}
                  iconColor={"#Ffa500"}/>

                <DashboardCard 
                  style={localStyles.cardRight}
                  count={dashboardData?.review || 0}
                  lable={'Reviews'}
                  circleColor={'#e6edff'}
                  iconName={'ios-thumbs-up-outline'}
                  iconColor={"#004cff"}/>
              </View>
            </View>
          </ScrollView>
      </SafeAreaView>
      <Progress showProgress={showProgress}/>
    </View>
  );
};

const localStyles = ScaledSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: color.white
  },
  scrollContainer: {
    width: '100%',
    padding: "10@ms"
  },
  cardContiner: {
    width: '100%',
    flexDirection: 'row',
    marginBottom: "15@ms"
  },
  cardRight: {
    marginStart: "10@ms",
  },
})

export default DashboardScreen;
