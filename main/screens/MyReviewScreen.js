import React, { useState, useEffect } from 'react';
import {FlatList, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import { SafeAreaView } from 'react-native-safe-area-context';

import {myReviewsRequest} from '../api/HandleRequest';
import color from '../resources/color';
import Constants from '../common/Constants';
import HeaderComponent from '../components/HeaderComponent';
import ReviewComponent from '../components/ReviewComponent';
import Divider9 from '../components/Divider9';
import UserModal from '../modal/UserModal';
import MyReviewsModal from '../modal/MyReviewsModal';
import Progress from '../components/Progress';
import { useSelector } from 'react-redux';
import { getUserData } from '../store/AppReducer';

const MyReviewScreen = props => {

  const userData = new UserModal(useSelector(state => getUserData(state)));
  const [data, setData] = useState([]);
  const [showProgress, setShowProgress] = useState(true);

  useEffect(() => {
    myReviewsRequest({payload: {"staffid": userData.id}}).then(async (result) => {
      const reviews = result?.body?.review?.reviews || [];
      const tempData = []
      for(let i=0; i<reviews.length; i++){
        tempData.push(new MyReviewsModal(reviews[i]));
      }
      setData(tempData);
      setShowProgress(false)

    }).catch(e => setShowProgress(false));
  }, []);

  return (
    <View style={localStyles.container}>
      <SafeAreaView>
        <HeaderComponent
          showBackButton
          title={Constants.review_rating}
          description={Constants.reviewDec}
          onPress={() => { props.navigation.pop()}}/>
        
        <Divider9 />
        
        <FlatList
          contentContainerStyle={localStyles.extraPadding}
          keyExtractor={(item, index) => {return index.toString()}}
          data={data}
          renderItem={({item, index}) => <ReviewComponent index={index} item={item}/>}/>
      </SafeAreaView>
      <Progress showProgress={showProgress}/>
    </View>
  );
}

const localStyles = ScaledSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: color.white,
  },
  extraPadding: {
    paddingBottom: "180@vs"
  }
});

export default MyReviewScreen;
