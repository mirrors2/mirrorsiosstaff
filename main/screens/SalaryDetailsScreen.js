import React from 'react';
import {View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {SafeAreaView} from 'react-native-safe-area-context';

import color from '../resources/color';

import HeaderComponent from '../components/HeaderComponent';

import Divider9 from '../components/Divider9';
import Constants from '../common/Constants';
import TextView from '../utils/TextView';

const SalaryDetailsScreen = props => {

  const params = props.route.params;
  const data = params.data;

  return (
    <View style={localStyles.container}>
      <SafeAreaView>
        <HeaderComponent
          showBackButton
          title={Constants.salaryDetails}
          description={Constants.salaryDec}
          onPress={() => {
            props.navigation.pop();
          }}
        />
        <Divider9 />
        <View style={localStyles.itemContainer}>
            <TextView style={{...localStyles.itemText, color: color.black}} text={params.item.monthYear}/>
            <TextView style={localStyles.valueText} text={params.item.totalPrice}/>
        </View>
        <Divider9 />

        <View style={localStyles.itemContainer}>
          <TextView style={{...localStyles.itemText, color: color.black}} text={Constants.particular}/>
          <TextView style={{...localStyles.valueText, textTransform: 'uppercase', color: color.black}} text={Constants.amount}/>
        </View>
        <View style={localStyles.itemContainer}>
          <TextView style={localStyles.itemText} text={Constants.salary}/>
          <TextView style={localStyles.valueText} text={data.salary}/>
        </View>
        <View style={localStyles.itemContainer}>
          <TextView style={localStyles.itemText} text={Constants.fine}/>
          <TextView style={localStyles.valueText} text={data.fine}/>
        </View>
        <View style={localStyles.itemContainer}>
          <TextView style={localStyles.itemText} text={Constants.advance}/>
          <TextView style={localStyles.valueText} text={data.advance}/>
        </View>
        <View style={localStyles.itemContainer}>
          <TextView style={{...localStyles.itemText, color: color.black, textAlign: 'right', flex: 1.3}} text={Constants.total}/>
          <TextView style={{...localStyles.valueText, color: color.black}} text={data.totalAmount}/>
        </View>
        <View style={localStyles.itemContainer}>
          <TextView style={{...localStyles.itemText, color: color.black, textAlign: 'right', flex: 1.3}} text={Constants.salaryStatus}/>
          <TextView style={{...localStyles.valueText, color: color.targetAchieved}} text={data.status}/>
        </View>
      </SafeAreaView>
    </View>
  );
};

const localStyles = ScaledSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: color.white,
  },
  itemContainer: {
    borderBottomWidth: '1.5@vs',
    borderBottomColor: '#f5f5f5',
    padding: '10@ms',
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemText: {
    flex: 1,
    fontFamily: 'HeliosAntique-Bold',
    fontSize: '12@ms',
    marginHorizontal: '5@ms',
    marginVertical: '2@ms',
    color: color.lead_disable_gray_color,
  },
  valueText: {
    flex: 1,
    fontFamily: 'HeliosAntique-Bold',
    fontSize: '12@ms',
    marginHorizontal: '5@ms',
    marginVertical: '2@ms',
    color: color.lead_disable_gray_color,
    textAlign: 'right',
  },
});

export default SalaryDetailsScreen;