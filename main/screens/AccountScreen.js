import React, { useEffect, useState } from 'react';
import {View, ScrollView, TouchableOpacity} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScaledSheet } from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/Ionicons';

import {removeAll} from '../utils/SharedPreferences';
import Divider9 from '../components/Divider9';
import HeaderComponent from '../components/HeaderComponent';
import color from '../resources/color';
import Constants from '../common/Constants';
import TextView from '../utils/TextView';
import ImageView from '../utils/ImageView';
import PreferenceKeys from '../common/PreferenceKeys';
import UserModal from '../modal/UserModal';
import { useSelector } from 'react-redux';
import { getUserData } from '../store/AppReducer';

const AccountScreen = props => {

  const userData = new UserModal(useSelector(state => getUserData(state)));
  const [imageError, setImageError] = useState(false);

  const handleProfile = () => {
    props.navigation.navigate('Profile', {userData});
  }

  const handleDashboard = () => {
    props.navigation.navigate(Constants.dashboard)
  }
  
  const handleAssignedServices = () => {
    props.navigation.navigate('Services');
  }
  
  const handleNewLead = () => {
    props.navigation.navigate(Constants.newLeads)
  }
  
  const handleOngoingLead = () => {
    props.navigation.navigate(Constants.ongoingLeads)
  }
  
  const handleCommission = () => {
    props.navigation.navigate('CSSPT', {name: Constants.commission});
  }
  
  const handleSalary = () => {
    props.navigation.navigate('CSSPT', {name: Constants.salary});
  }
  
  const handleServiceSale = () => {
    props.navigation.navigate('CSSPT', {name: Constants.serviceSale});
  }
  
  const handleProductSale = () => {
    props.navigation.navigate('CSSPT', {name: Constants.productSale});
  }
  
  const handleTarget = () => {
    props.navigation.navigate('CSSPT', {name: Constants.target});
  }
  
  const handleBankDetails = () => {
    props.navigation.navigate('BankDetails');
  }
  
  const handleMyReviews = () => {
    props.navigation.navigate('MyReviews');
  }

  const handleLogout = async () => {
    props.navigation.removeListener('tabPress')
    await removeAll(Object.keys(PreferenceKeys));
    props.navigation.replace('Login')
  }

  return (
    <View style={localStyles.container}>
      <SafeAreaView>
        <HeaderComponent 
          title={Constants.account}
          description={Constants.accountDec}/>
          <Divider9 />

          <ScrollView>
            <View style={localStyles.scrollContainer}>

              <TouchableOpacity onPress={() => handleProfile()}>
                <View style={localStyles.profileContainer}>
                  <ImageView 
                    style={localStyles.profileImage}
                    resizeMode={'contain'}
                    image={imageError ? require('../resources/drawable/profile.png') : {uri: userData.image}}
                    onError={err => setImageError(true)}/>

                    <View style={localStyles.staffTextContainer}>
                      <TextView style={localStyles.staffNameText} text={userData.name}/>
                      <TextView style={localStyles.staffDesignationText} text={userData.specialist}/>
                    </View>
                    <Icon name='chevron-forward-outline' size={localStyles.arrowIconSize} color={color.edit_account}/>
                </View>
              </TouchableOpacity>

              <Divider9 />

              <TouchableOpacity onPress={() => handleDashboard()}>
                <View style={localStyles.itemContainer}>
                  <View style={localStyles.item}>
                    <TextView style={localStyles.itemText} text={Constants.dashboard}/>
                    <Icon name='chevron-forward-outline' size={localStyles.arrowIconSize} color={color.edit_account}/>
                  </View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => handleAssignedServices()}>
                <View style={localStyles.itemContainer}>
                  <View style={localStyles.item}>
                    <TextView style={localStyles.itemText} text={Constants.assignedCategoryServices}/>
                    <Icon name='chevron-forward-outline' size={localStyles.arrowIconSize} color={color.edit_account}/>
                  </View>
                </View>
              </TouchableOpacity>

              <Divider9 />

              <TouchableOpacity onPress={() => handleNewLead()}>
                <View style={localStyles.itemContainer}>
                  <View style={localStyles.item}>
                    <TextView style={localStyles.itemText} text={Constants.newAppointment}/>
                    <Icon name='chevron-forward-outline' size={localStyles.arrowIconSize} color={color.edit_account}/>
                  </View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => handleOngoingLead()}>
                <View style={localStyles.itemContainer}>
                  <View style={localStyles.item}>
                    <TextView style={localStyles.itemText} text={Constants.ongoingAppointment}/>
                    <Icon name='chevron-forward-outline' size={localStyles.arrowIconSize} color={color.edit_account}/>
                  </View>
                </View>
              </TouchableOpacity>

              <Divider9 />

              <TouchableOpacity onPress={() => handleCommission()}>
                <View style={localStyles.itemContainer}>
                  <View style={localStyles.item}>
                    <TextView style={localStyles.itemText} text={Constants.commission}/>
                    <Icon name='chevron-forward-outline' size={localStyles.arrowIconSize} color={color.edit_account}/>
                  </View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => handleSalary()}>
                <View style={localStyles.itemContainer}>
                  <View style={localStyles.item}>
                    <TextView style={localStyles.itemText} text={Constants.salary}/>
                    <Icon name='chevron-forward-outline' size={localStyles.arrowIconSize} color={color.edit_account}/>
                  </View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => handleServiceSale()}>
                <View style={localStyles.itemContainer}>
                  <View style={localStyles.item}>
                    <TextView style={localStyles.itemText} text={Constants.serviceSale}/>
                    <Icon name='chevron-forward-outline' size={localStyles.arrowIconSize} color={color.edit_account}/>
                  </View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => handleProductSale()}>
                <View style={localStyles.itemContainer}>
                  <View style={localStyles.item}>
                    <TextView style={localStyles.itemText} text={Constants.productSale}/>
                    <Icon name='chevron-forward-outline' size={localStyles.arrowIconSize} color={color.edit_account}/>
                  </View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => handleTarget()}>
                <View style={localStyles.itemContainer}>
                  <View style={localStyles.item}>
                    <TextView style={localStyles.itemText} text={Constants.target}/>
                    <Icon name='chevron-forward-outline' size={localStyles.arrowIconSize} color={color.edit_account}/>
                  </View>
                </View>
              </TouchableOpacity>

              <Divider9 />

              <TouchableOpacity onPress={() => handleBankDetails()}>
                <View style={localStyles.itemContainer}>
                  <View style={localStyles.item}>
                    <TextView style={localStyles.itemText} text={Constants.bankDetails}/>
                    <Icon name='chevron-forward-outline' size={localStyles.arrowIconSize} color={color.edit_account}/>
                  </View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => handleMyReviews()}>
                <View style={localStyles.itemContainer}>
                  <View style={localStyles.item}>
                    <TextView style={localStyles.itemText} text={Constants.myReviews}/>
                    <Icon name='chevron-forward-outline' size={localStyles.arrowIconSize} color={color.edit_account}/>
                  </View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity style={localStyles.logoutContainer} onPress={() => handleLogout()}>
                <TextView 
                  style={localStyles.logoutText}
                  text={Constants.logout}/>
              </TouchableOpacity>
            </View>
          </ScrollView>
      </SafeAreaView>
    </View>
  );
};


const localStyles = ScaledSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: color.white
  },
  scrollContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
    paddingBottom: "80@vs"
  },
  profileContainer: {
    padding: "10@ms",
    flexDirection: 'row',
    alignItems: 'center'
  },
  profileImage: {
    width: '55@ms',
    height: '55@ms',
    resizeMode: 'contain',
    borderColor: color.light_grey,
    borderRadius: "100@ms",
    borderWidth: '1.5@ms'
  },
  staffTextContainer: {
    flex: 1,
    marginHorizontal: "10@ms"
  },
  staffNameText: {
    fontFamily: 'HeliosAntique-Bold',
    fontSize: "18@ms"
  },
  staffDesignationText: {
    fontFamily: "HeliosAntique-Regular",
    fontSize: "13@ms",
    marginTop: "3@ms"
  },
  itemContainer: {
    borderBottomWidth: "1.5@vs", 
    borderBottomColor: '#f5f5f5'
  },
  item: {
    margin: "10@ms",
    flexDirection: 'row',
  },
  itemText: {
    flex: 1,
    fontFamily: "HeliosAntique-Regular",
    fontSize: "13@ms",
    marginHorizontal: "5@ms",
    marginVertical: "2@ms",
    color: color.black
  },
  arrowIconSize: "20@ms",
  logoutContainer: {
    marginVertical: "30@vs",
    paddingHorizontal: "10@ms",
    alignSelf: 'center'
  },
  logoutText: {
    fontFamily: "HeliosAntique-Regular",
    fontWeight: "600",
    fontSize: "15@ms",
    color: color.red
  }
})


export default AccountScreen;
