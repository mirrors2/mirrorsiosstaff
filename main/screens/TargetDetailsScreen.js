import React from 'react';
import {FlatList, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {SafeAreaView} from 'react-native-safe-area-context';

import color from '../resources/color';

import HeaderComponent from '../components/HeaderComponent';
import TargetItemComponent from '../components/TargetItemComponent';

import Divider9 from '../components/Divider9';
import Constants from '../common/Constants';
import TextView from '../utils/TextView';

const TargetDetailsScreen = props => {
  
  const params = props.route.params;
  const data = params.data;

  return (
    <View style={localStyles.container}>
      <SafeAreaView>
        <HeaderComponent
          showBackButton
          title={Constants.targetDetails}
          description={params?.monthYear || ''}
          onPress={() => {
            props.navigation.pop();
          }}
        />
        <Divider9 />
        <View>
          <View style={localStyles.textContainer}>
            <TextView style={localStyles.targetAchievedText} text={Constants.targetReached} />
            <TextView style={localStyles.targetText} text={data.targetreached} />
          </View>
          <Divider9 />
          <FlatList
            contentContainerStyle={localStyles.extraPadding}
            keyExtractor={(item, index) => {return index.toString()}}
            data={data.targets}
            renderItem={({index, item}) => <TargetItemComponent index={index} data={item} />}
          />
        </View>
      </SafeAreaView>
    </View>
  );
};

const localStyles = ScaledSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: color.white,
  },
  textContainer: {
    margin: "10@ms"
  },
  targetAchievedText: {
    fontFamily: 'HeliosAntique-Bold',
    fontSize: "11@ms",
    color: color.account_options
  },
  targetText: {
    fontFamily: 'HeliosAntique-Bold',
    fontSize: "15@ms",
    color: color.black,
    marginTop: "5@ms"
  },
  extraPadding: {
    paddingBottom: "180@vs"
  }
});

export default TargetDetailsScreen;
