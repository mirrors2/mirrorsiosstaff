import React, { useState } from 'react';
import {Dimensions, Platform, ScrollView, Share, View} from 'react-native';
import Pdf from 'react-native-pdf';
import { SafeAreaView } from 'react-native-safe-area-context';
import {ScaledSheet} from 'react-native-size-matters';

import Constants from '../common/Constants';
import Button from '../components/Button';
import HeaderComponent from '../components/HeaderComponent';
import Progress from '../components/Progress';
import color from '../resources/color';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const InvoiceScreen = props => {
  const pdfUri = {uri: props.route.params.link, cache: true};
  const [showProgress, setShowProgress] = useState(true);
  const [filePath, setFilePath] = useState('');

  const sharePDF = async () => {
    try{
        let options = {
            type: 'application/pdf',
            url: Platform.OS === 'android' ? 'file://' + filePath : filePath
        };
        await Share.share(options);
    }catch(e){alert(e); console.log(e)}
  }

  return (
    <View style={localStyles.container}>
      <SafeAreaView>
        <HeaderComponent
          showBackButton
          title={Constants.invoiceDetails}
          onPress={() => {
            props.navigation.pop();
          }}
        />

       <View style={localStyles.pdfContainer}>
           <Pdf
               source={pdfUri}
               onLoadComplete={(numberOfPages, filePath) => {
                   setFilePath(filePath);
                   setShowProgress(false);
               }}
               onPageChanged={(page, numberOfPages) => {}}
               onError={error => {
                   alert(Constants.unexpectedError);
               }}
               onPressLink={uri => {
                   console.log(`Link pressed: ${uri}`);
               }}
               style={localStyles.pdf}
           />
           <Button
               text={Constants.share}
               onPress={() => sharePDF()}
               style={localStyles.invoiceButton}
               textStyle={{textTransform: "uppercase"}}/>
       </View>

      </SafeAreaView>
      <Progress showProgress={showProgress} />
    </View>
  );
};

const localStyles = ScaledSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: color.white,
  },
pdfContainer: {
    width: width
},
  pdf: {
    width: width,
    height: height*0.75,
  },
  invoiceButton: {
    marginHorizontal: "10@ms",
    marginTop: "20@vs",
  },
});

export default InvoiceScreen;
