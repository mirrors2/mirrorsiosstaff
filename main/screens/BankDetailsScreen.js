import React from 'react';
import {View, ScrollView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import { SafeAreaView } from 'react-native-safe-area-context';

import color from '../resources/color';
import Constants from '../common/Constants';
import HeaderComponent from '../components/HeaderComponent';
import Divider9 from '../components/Divider9';
import TextView from '../utils/TextView';
import BankModal from '../modal/BankModal';
import ImageView from '../utils/ImageView';
import { getBankData } from '../store/AppReducer';
import { useSelector } from 'react-redux';

const BankDetailsScreen = props => {

  const bankData = new BankModal(useSelector(state => getBankData(state)));


  return (
    <View style={localStyles.container}>
      <SafeAreaView>
        <HeaderComponent
          showBackButton
          title={Constants.bankDetails}
          description={Constants.bankDec}
          onPress={() => { props.navigation.pop()}}/>
        <Divider9 />
        <ScrollView>
          <View style={localStyles.scrollContainer}>
            <View style={{...localStyles.itemContainer, flexDirection: 'row', alignItems: 'center'}}>
              <View style={{flex: 1}}>
                <TextView style={localStyles.itemText} text={Constants.bankName} />
                <TextView style={localStyles.valueText} text={bankData.bankName} />
              </View>
              <ImageView 
                style={localStyles.image}
                resizeMode={'stretch'}
                image={{uri: bankData.image}}/>
            </View>
            <View style={localStyles.itemContainer}>
              <TextView style={localStyles.itemText} text={Constants.accountNumber} />
              <TextView style={localStyles.valueText} text={bankData.accountNumber} />
            </View>
            <View style={localStyles.itemContainer}>
              <TextView style={localStyles.itemText} text={Constants.branch} />
              <TextView style={localStyles.valueText} text={bankData.branch} />
            </View>
            <View style={localStyles.itemContainer}>
              <TextView style={localStyles.itemText} text={Constants.branchCode} />
              <TextView style={localStyles.valueText} text={bankData.branchCode} />
            </View>
            <View style={localStyles.itemContainer}>
              <TextView style={localStyles.itemText} text={Constants.ibanNumber} />
              <TextView style={localStyles.valueText} text={bankData.ibanNumber} />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

const localStyles = ScaledSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: color.white,
  },
  scrollContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
    paddingBottom: '60@vs',
  },
  itemContainer: {
    borderBottomWidth: '1.5@vs',
    borderBottomColor: '#f5f5f5',
    padding: '10@ms',
  },
  itemText: {
    flex: 1,
    fontFamily: 'HeliosAntique-Bold',
    fontSize: '12@ms',
    marginHorizontal: '5@ms',
    marginVertical: '2@ms',
    color: color.black,
    textTransform: 'uppercase',
  },
  valueText: {
    flex: 1,
    fontFamily: "HeliosAntique-Regular",
    fontSize: '12@ms',
    marginHorizontal: '5@ms',
    marginVertical: '2@ms',
    color: color.account_options,
  },
  image: {
    width: '20%', 
    height: "30@vs"
  }
});

export default BankDetailsScreen;
