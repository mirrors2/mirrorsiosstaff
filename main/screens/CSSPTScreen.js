import React, { useState, useEffect } from 'react';
import {FlatList, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {SafeAreaView} from 'react-native-safe-area-context';
import moment from 'moment';

import {commissionRequest, salaryTargetDetailsRequest} from '../api/HandleRequest';
import color from '../resources/color';

import HeaderComponent from '../components/HeaderComponent';
import YearComponent from '../components/YearComponent';
import CSSPTComponent from '../components/CSSPTComponent';
import Divider9 from '../components/Divider9';

import UserModal from '../modal/UserModal';
import CSSPTModal from '../modal/CSSPTModal';
import SalaryDetailsModal from '../modal/SalaryDetailsModal';
import TargetDetailsModal from '../modal/TargetDetailsModal';

import Progress from '../components/Progress';
import Constants from '../common/Constants';
import { getCurrency, getUserData } from '../store/AppReducer';
import { useSelector } from 'react-redux';

const CSSPTScreen = props => {

  const state = useSelector(state => state);
  const currency = getCurrency(state);
  const userData = new UserModal(getUserData(state));

  const headerName = props.route?.params?.name;
  const [years, setYears] = useState([]);
  const [data, setData] = useState([]);
  const [showProgress, setShowProgress] = useState(false);
  const [selectedYear, setSelectedYear] = useState('');
  const [iconName, setIconName] = useState()

  useEffect(() => {
    getIconName();

    let tempData = [];
    const currentYear = moment().year();
    try{

      let startYear = moment(userData.startDate,"DD/MM/YYYY").year();
      if(isNaN(startYear)) startYear = moment(userData.startDate,"YYYY-MM-DD").year();

      for(let i=currentYear; i>=startYear; i--){
        tempData.push(i);
      }
    }catch(e) { tempData[0] = currentYear}
    setYears(tempData)
    handleSelectedYear(userData, currentYear)
  }, []);

  const handleSelectedYear = async (userModal, item) => {
    setShowProgress(true)
    setSelectedYear(item);
    const payload = {staffid: userModal.id, year: item};
    try{
        const responseModal = await commissionRequest(headerName, {payload: payload});
        if(!responseModal.status){
            const details = responseModal.body?.details || [];
            let tempData = [];
            for(let i=0; i<details.length; i++){
                tempData.push( new CSSPTModal(headerName, details[i], currency));
            }
            setData(tempData)
            if(tempData.length === 0) alert(Constants.recordsNotFound);
        }else{
            alert(Constants.recordsNotFound);
        }
    }catch(e){
        alert(Constants.recordsNotFound);
        console.log(e)
    }
    setShowProgress(false)
  }

  const handleItemPress = (item) => {
    if(headerName === Constants.salary && item.detail === 'Y' ){
      handleDetailsRequest(Constants.salaryDetails, item);
      
    }else if(headerName === Constants.target){
      handleDetailsRequest(Constants.targetDetails, item);
    }
  }

  const handleDetailsRequest = async (type, item) => {
    setShowProgress(true)
    try{
      const payload = {staffid: userData.id, year: item.year, month: item.month};
      const responseModal = await salaryTargetDetailsRequest(type, {payload: payload});
      if(!responseModal.isStaus){
        if(type === Constants.salaryDetails){
          const modal = new SalaryDetailsModal(responseModal.body?.details[0], currency)
          props.navigation.navigate("SalaryDetails", {data: modal, item})
        }else{
          const modal = new TargetDetailsModal(responseModal.body, currency)
          props.navigation.navigate("TargetDetails", {data: modal, monthYear: item.monthYear})
        }
      }
    }catch(e){ }
    setShowProgress(false)
  }

  const getIconName = () => {
    if([Constants.commission, Constants.salary].includes(headerName)){
        setIconName('newspaper-outline');
    }else if(headerName === Constants.serviceSale){
        setIconName('stats-chart-outline');
    }else if(headerName === Constants.productSale){
        setIconName('clipboard-outline');
    }else if(headerName === Constants.target){
        setIconName('ios-disc-outline');
    }
  }

  return (
    <View style={localStyles.container}>
      <SafeAreaView>
        <HeaderComponent
          showBackButton
          title={headerName}
          description={`You can see you ${headerName} here`}
          onPress={() => {
            props.navigation.pop();
          }}
        />
        <Divider9 />
        <View>
            <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={localStyles.yearList}
                keyExtractor={(item, index) => {return index.toString()}}
                data={years}
                renderItem={({item}) => 
                    <YearComponent 
                        selectedYear={selectedYear}
                        text={item} 
                        onPress={() => handleSelectedYear(userData, item)}
                    />
                }
            />
            <Divider9 />
            <FlatList
                contentContainerStyle={localStyles.extraPadding}
                keyExtractor={(item, index) => {return index.toString()}}
                data={data}
                renderItem={({item, index}) =>
                    <CSSPTComponent
                        {...props}
                        key={index}
                        type={headerName}
                        iconName={iconName}
                        data={item}
                        onPress={() => handleItemPress(item)}
                    />
                }
            />
        </View>
      </SafeAreaView>
      <Progress showProgress={showProgress}/>
    </View>
  );
};

const localStyles = ScaledSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: color.white,
  },
  yearList: {
      margin: "10@ms",
      paddingEnd: "20@ms"
  },
  extraPadding: {
    paddingBottom: "280@vs"
  }
});

export default CSSPTScreen;
