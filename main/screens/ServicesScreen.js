import React, {useState, useEffect} from 'react';
import {View, FlatList} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import { SafeAreaView } from 'react-native-safe-area-context';

import {servicesRequest} from '../api/HandleRequest';
import color from '../resources/color';
import Constants from '../common/Constants';
import UserModal from '../modal/UserModal';

import HeaderComponent from '../components/HeaderComponent';
import CategoryComponent from '../components/CategoryComponent';
import Divider9 from '../components/Divider9';
import TextView from '../utils/TextView';
import Progress from '../components/Progress';
import { useSelector } from 'react-redux';
import { getUserData } from '../store/AppReducer';

const ServicesScreen = props => {

  const userData = new UserModal(useSelector(state => getUserData(state)));
  const [categories, setCategories] = useState(Constants.categories);
  const [data, setData] = useState([]);
  const [showProgress, setShowProgress] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState('');
  
  useEffect(() => {
    handleSelectedCategory(userData, categories[0])
  }, []);

  const handleSelectedCategory = async (userModal, item) => {
    setShowProgress(true)
    setSelectedCategory(item.name);
    const payload = {staffid: userModal.id, category: item.id};
    try{
        const responseModal = await servicesRequest({payload: payload});
        if(!responseModal.status){
            setData(responseModal?.body || [])
        }else{
          alert(responseModal.message);
          setData([])
        }
    }catch(e){
      setData([])
      console.log(e)
    }
    setShowProgress(false)
  }

  const serviceItem = (item) => {
    return(
      <View style={localStyles.itemContainer}>
        <TextView style={localStyles.itemText} text={item.servicename}/>
      </View>
    )
  }
  return (
    <View style={localStyles.container}>
      <SafeAreaView>
        <HeaderComponent
          showBackButton
          title={Constants.service}
          description={Constants.serviceDec}
          onPress={() => { props.navigation.pop()}}/>
        <Divider9 />

        <View>
          <FlatList 
              horizontal
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={localStyles.yearList}
              keyExtractor={(item, index) => {return index.toString()}}
              data={categories}
              renderItem={({item}) => 
                  <CategoryComponent 
                      selectedCategory={selectedCategory}
                      data={item} 
                      onPress={() => handleSelectedCategory(userData, item)}
                  />
              }
          />
          <Divider9 />
          <FlatList
                contentContainerStyle={localStyles.extraPadding}
                keyExtractor={(item, index) => {return index.toString()}}
                data={data}
                renderItem={({item, index}) => serviceItem(item)}
            />
        </View>
      </SafeAreaView>

      <Progress showProgress={showProgress}/>
    </View>
  );
};

const localStyles = ScaledSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: color.white,
  },
  yearList: {
    margin: "10@ms",
    paddingEnd: "20@ms"
  },
  itemContainer: {
    borderBottomWidth: "1.5@vs", 
    borderBottomColor: '#f5f5f5',
    padding: "10@ms",
  },
  itemText: {
    flex: 1,
    fontFamily: "HeliosAntique-Regular",
    fontSize: "13@ms",
    marginHorizontal: "5@ms",
    marginVertical: "2@ms",
    color: color.black
  },
  extraPadding: {
    paddingBottom: "280@vs"
  }
});

export default ServicesScreen;
