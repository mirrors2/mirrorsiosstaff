import React, {useEffect} from 'react';
import {View, StyleSheet} from 'react-native';

import {getAll} from '../utils/SharedPreferences';
import { useDispatch } from 'react-redux';

import ImageView from '../utils/ImageView';
import PreferenceKeys from '../common/PreferenceKeys';

const SplashScreen = props => {

  const dispatch = useDispatch();
  useEffect(() => {
    
    setTimeout(async () => {
      const prefData = await getAll(Object.keys(PreferenceKeys))
      try{
        const userData = JSON.parse(prefData[0][1]);
        if(userData){
          const bankData = JSON.parse(prefData[1][1]);
          const currency = JSON.parse(prefData[2][1]);
          const badgeCount = JSON.parse(prefData[3][1]);

          dispatch({type: 'userData', userData})
          dispatch({type: 'bankData', bankData})
          dispatch({type: 'badgeCount', badgeCount})
          dispatch({type: 'currency', currency})
          props.navigation.replace("Tabs")
        }else{
          props.navigation.replace("Login")
        }
      }catch(e){
        props.navigation.replace("Login")
      }
    }, 3000);

  }, []);
  
  return(
    <View style={localStyles.container}>
        <ImageView 
          style={localStyles.container}
          resizeMode={'stretch'}
          image={require('../resources/drawable/splash_background.jpeg')}
        />
    </View>
  )
};

const localStyles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%'
  },
});

export default SplashScreen;
