import React, {useState} from 'react';
import {ScrollView, View, TextInput, KeyboardAvoidingView, FlatList, Linking, Platform, Modal, TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import {s, ScaledSheet} from 'react-native-size-matters';
import {SafeAreaView} from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';
import MapView, { Marker, PROVIDER_DEFAULT } from 'react-native-maps'; 
import moment from 'moment';
import {Rating} from 'react-native-ratings';

import {leadDetailsRequest, startStopRequest, postReviewRequest, receivedPaymentRequest, downloadInvoiceRequest} from '../api/HandleRequest';

import color from '../resources/color';
import Constants from '../common/Constants';
import HeaderComponent from '../components/HeaderComponent';
import ImageView from '../utils/ImageView';
import TextView from '../utils/TextView';
import Divider9 from '../components/Divider9';
import UserModal from '../modal/UserModal';
import LeadDetailsModal from '../modal/LeadDetailsModal';
import Button from '../components/Button';
import CustomerServiceComponent from '../components/CustomerServiceComponent';
import Progress from '../components/Progress';
import { getDate } from '../utils/DateUtil';
import { useSelector } from 'react-redux';
import { getCurrency, getUserData } from '../store/AppReducer';


const LeadDetailsScreen = props => {
  const params = props.route.params;
  const showButtons = params.item?.type !== 'Cancelled';
  const currentDate = moment(Date.now()).format('YYYY-MM-DD HH:mm');

  const state = useSelector(state => state);
  const currency = getCurrency(state);
  const userData = new UserModal(getUserData(state));

  const [details, setDetails] = useState(new LeadDetailsModal(params.data, currency));
  const [imageError, setImageError] = useState(false);
  const [isCash, setCash] = useState(false);
  const [isCard, setCard] = useState(false);
  const [authCode, setAuthCode] = useState('');
  const [showProgress, setShowProgress] = useState(false);
  const [showWriteReviewModal, setShowWriteReviewModal] = useState(false)
  const [showPaymentAlertModal, setShowPaymentAlertModal] = useState(false);
  const [shouldUpdate, setShouldUpdate] = useState(false);
  const [rating, setRating] = useState(0);
  const [emoji, setEmoji] = useState(require('../resources/star1.png'));
  const [comment, setComment] = useState('');

  const handleDate = () => {
    return getDate(currentDate, details.date, details.time);
  }

  const callNumber = () => {
    try{
      let phoneNumber = details.contact;
      if (Platform.OS === 'android') {
        phoneNumber = `tel:${phoneNumber}`;
      }else  {
        phoneNumber = `tel://${phoneNumber.split('tel:').pop()}`;
      }
      Linking.openURL(phoneNumber);
    }catch(e){
      alert(Constants.unexpectedError);
    }
    
  };

  const openMaps = () => {
    try{
      const latLng = `${details.latLng.latitude}+${details.latLng.longitude}`;
      if (Platform.OS === 'android') {
        Linking.openURL(`google.navigation:q=${latLng}`)
      }else{
        Linking.openURL(`maps://app?saddr=&daddr=${latLng}`);
      }

    }catch(e){
      alert(Constants.unexpectedError);
    }
  }

  const getRatingIcon = () => {
    // if(rating === 1) return '../resources/star5.png'

    return 'star5.png'
  }
  const handleStartStop = async (type, item) => {
    setShowProgress(true)
    try{
      const payload = {type: type, cartid: item.cartId};
      const responseModal = await startStopRequest({payload: payload});
      if(responseModal.status === "FAILED"){
          alert(responseModal.message)
          setShowProgress(false)
      }else{
        handleLeadDetails();
      }
    }catch(e){
      alert(Constants.unexpectedError);
      console.log(e);
      setShowProgress(false);
    }
    
  }

  const handlePostReview = async (rating, review) => {
    setShowProgress(true)
    try{
      const payload = {
        staffid: userData.id, customer_id: details.customerId, customer_name: details.customerName, 
        refid: details.refId, star: rating, review: review, loc: details.location
      }

      const responseModal = await postReviewRequest({payload: payload});
      if(responseModal.status === "FAILED"){
          alert(responseModal.message)
          setShowProgress(false)
      }else{
        setShowWriteReviewModal(false);
        handleLeadDetails()   
      }
    }catch(e){
      alert(Constants.unexpectedError);
      console.log(e);
      setShowProgress(false);
    }
    
  }

  const handleReceivedPayment = async (showAlert) => {
    if(!isCash && !isCard) return alert(Constants.pleaseSelectPaymentType)
    if(isCard && authCode === '') return alert(Constants.pleaseEnterAuthCode)
    
    setShowPaymentAlertModal(showAlert);
    if(showAlert) return

    setShowProgress(true)
    try{
      
      const payload = {
        customerid: details.customerId, 
        customer_name: details.customerName, 
        ref_id: details.refId, payment_type: isCash ? "Cash" : "Card", 
        price: details.amount.replace("AED", ""), 
        authcode: authCode, 
        staff_id: userData.id,
        staff_name: userData.name
      };
      const responseModal = await receivedPaymentRequest({payload: payload});
      if(responseModal.status === "FAILED"){
          alert(responseModal.message)
          setShowProgress(false);
      }else{
        handleLeadDetails();
      }
    }catch(e){
      alert(Constants.unexpectedError);
      console.log(e);
      setShowProgress(false)
    }
  }

  const handleInvoice = async () => {
    setShowProgress(true)
    try{
      const payload = {refid: details.refId};
      const responseModal = await downloadInvoiceRequest({payload: payload});
      if(responseModal.status === "FAILED"){
          alert(responseModal.message)
      }else{
        const link = responseModal.body?.link || '';
        props.navigation.navigate('Invoice', {link});
      }
    }catch(e){
      alert(Constants.unexpectedError);
      console.log(e)
    }
    setShowProgress(false)
  }

  const handleRating = (val) => {
    setRating(val);

    if(val > 4) return setEmoji(require(`../resources/star5.png`));
    if(val > 3) return setEmoji(require(`../resources/star4.png`));
    if(val > 2) return setEmoji(require(`../resources/star3.png`));
    if(val > 1) return setEmoji(require(`../resources/star2.png`));
    if(val > 0) return setEmoji(require(`../resources/star1.png`));
  }
  const handlePaymentType = (type) => {
    const flag = type === Constants.cash;
    setCash(flag);
    setCard(!flag);
  }

  const handleLeadDetails = async () => {
    try{
      const payload = {staffid: userData.id, refid: details.refId};
      const responseModal = await leadDetailsRequest({payload: payload});
      if(responseModal.status === "FAILED"){
        alert(responseModal.message)
      }else{
        setShouldUpdate(true);
        setDetails(new LeadDetailsModal(responseModal.body, currency));
      }
    }catch(e){ 
      console.log(e);
      alert(Constants.unexpectedError)
    }
    setShowProgress(false)
    
  }

  const profileView = () => {
    return (
      <View style={localStyles.profileContainer}>
        <ImageView
          style={localStyles.profileImage}
          resizeMode={'contain'}
          image={ imageError ? require('../resources/drawable/profile.png') : {uri: details.image} }
          onError={err => setImageError(true)}/>
        <View style={localStyles.profileTextContainer}>
            <TextView style={localStyles.titleText} text={details.refId} />
            <TextView style={localStyles.descriptionText} text={details.customerName} />
        </View>
        <View style={localStyles.profileDateTextContainer}>
          <TextView style={localStyles.profileDateText} text={handleDate()} />
          <TextView style={localStyles.profileDateText} text={details.time} />
        </View>
        </View>
    )
  }

  const dateView = () => {
    return(
      <View style={localStyles.viewContainer}>
        <TextView style={localStyles.titleText} text={Constants.dateAndTime} />
        <TextView style={localStyles.descriptionText} text={`${details.date} ${details.time}`} />
      </View>
    )
  }

  const locationView = () => {
    return(
      <View style={localStyles.viewContainer}>
        <TextView style={localStyles.titleText} text={Constants.jobLocation} />
        <TextView style={localStyles.descriptionText} text={details.customerAddress} />
        <MapView
          provider={PROVIDER_DEFAULT} // remove if not using Google Maps
          style={localStyles.maps}
          region={{
            ...details.latLng,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}>
            <Marker
              key={0}
              coordinate={{...details.latLng}}
              title={details.customerAddress}
            />
        </MapView>

        <View style={localStyles.mapButtonContainer}>
          <Button
            text={Constants.viewMap}
            onPress={() => openMaps()}
            style={{...localStyles.viewMapButton, backgroundColor: color.primaryLightColor}}
            textStyle={{...localStyles.buttonTextStyle, color: color.finish}}/>
          <Button
            text={Constants.callToCustomer}
            onPress={() => callNumber()}
            style={{...localStyles.viewMapButton, ...localStyles.callCustomerButton}}
            textStyle={{...localStyles.buttonTextStyle, color: color.black}}/>
        </View>
      </View>
    )
  }

  const jobCompletedView = () => {
    return (
      <View style={localStyles.viewContainer}>
        <TextView style={localStyles.titleText} text={Constants.jobDetails} />
        <View style={localStyles.completedContainer}>
          <Icon name={'checkmark-circle'} size={25} color={color.start}/>
          <TextView style={localStyles.completedText} text={Constants.jobCompleted} />
        </View>
        {details?.reviewPending == 1  ? 
        <View style={{...localStyles.completedContainer, padding: 0, flexDirection: 'row', backgroundColor: color.primaryLightColor}}>
          <View style={{...localStyles.completedContainer, marginTop: 0, flex: 1, backgroundColor: color.primaryLightColor}}>
              <Icon name={'newspaper-outline'} size={25} color={color.finish}/>
              <TextView style={{...localStyles.completedText, color: color.finish}} text={Constants.pendingReview} />
          </View>
          <Button 
            text={Constants.write} 
            style={localStyles.writeReviewButton} 
            textStyle={localStyles.writeReviewButtonText}
            onPress={() => setShowWriteReviewModal(true)}/>
        </View>
        :
        <View style={localStyles.completedContainer}>
          <Icon name={'checkmark-circle'} size={25} color={color.start}/>
          <TextView style={localStyles.completedText} text={Constants.reviewCompleted} />
        </View>
        }
      </View>
    )
  }

  const reviewView = () => {
    return (
      <View style={localStyles.viewContainer}>
        <View style={localStyles.reviewTextContainer}>
          <TextView style={localStyles.titleText} text={Constants.reviews} />
          <FlatList
              contentContainerStyle={{justifyContent: 'flex-end', flex:1}}
              horizontal={true}
              data={Array(5)}
              renderItem={({index, item}) =>
                  <Icon
                      name={'star'}
                      size={localStyles.ratingStartSize}
                      color={index < (details.review?.star || 0) ? '#f1c40f' : color.disable_text_color}/> }/>
        </View>
        <TextView style={localStyles.descriptionText} text={details.review.review} />
      </View>
    )
  }

  const servicesView = () => {
    return(
      <View style={localStyles.viewContainer}>
        <TextView style={localStyles.titleText} text={Constants.customerServices} />
        {details.services.map(item => (
          <CustomerServiceComponent
            key={item.cartId}
            data={item}
            showButtons={showButtons}
            startOnPress={() => handleStartStop('start', item)}
            finishOnPress={() => handleStartStop('stop', item)}/>
        ))}
      </View>
    )
  }

  const amountView = () => {
    return (
      <View style={localStyles.viewContainer}>
        <TextView style={localStyles.titleText} text={Constants.amount} />
        <TextView style={localStyles.descriptionText} text={details.amount} />
      </View>
    )
  }

  const jobStartedView = () => {
    return (
      <View style={localStyles.viewContainer}>
        <TextView style={localStyles.titleText} text={Constants.jobStartedAt} />
        <TextView style={localStyles.descriptionText} text={details.jobStartTime} />
      </View>
    )
  }

  const jobEndedView = () => {
    return (
      <View style={localStyles.viewContainer}>
        <TextView style={localStyles.titleText} text={Constants.jobEndedAt} />
        <TextView style={localStyles.descriptionText} text={details.jobEndTime} />
      </View>
    )
  }

  const paymentView = () => {
    return (
      <View style={localStyles.viewContainer}>
        <TextView style={localStyles.titleText} text={Constants.paymentType} />
        {details.payment ? 
          <View>
            <View style={{flexDirection: 'row'}}>
              <View style={localStyles.paymentStatusContainer}>
                <TextView style={localStyles.paymentStatusText} text={Constants.successfullyReceivedPayment(details.payment.paymentType)} />
              </View>
            </View>
            {details.payment.paymentType?.toUpperCase() === Constants.card.toUpperCase() &&
                <TextView
                    style={{...localStyles.titleText, marginTop: 15}}
                    text={`${Constants.authCode} - ${details.payment?.code || ''}`} />
            }
            <Button
              text={Constants.downloadInvoice}
              onPress={() => handleInvoice()}
              style={localStyles.invoiceButton}
              textStyle={{textTransform: "uppercase"}}/>
          </View> :
          <View>
            <View style={localStyles.paymentTypeContainer}>
              <Button 
                text={Constants.cash}
                style={{...localStyles.startButton, backgroundColor: isCash ? color.start : color.completed}}
                textStyle={{...localStyles.startButtonText, color: isCash ? color.white : color.start}}
                onPress={() => handlePaymentType(Constants.cash)}
              />
              <Button 
                text={Constants.card}
                style={{...localStyles.startButton, backgroundColor: isCard ? color.start : color.completed}}
                textStyle={{...localStyles.startButtonText, color: isCard ? color.white : color.start}}
                onPress={() => handlePaymentType(Constants.card)}
              />
            </View>
            {isCard &&
              <View style={localStyles.authCodeContainer}>
                <TextView text={Constants.authCode} />
                <TextInput
                  style={localStyles.input}
                  placeholder={Constants.enter}
                  onChangeText={(value) => setAuthCode(value)}/>
              </View>
            }
            <Button
              text={Constants.receivedPayment}
              onPress={() => handleReceivedPayment(true)}
              style={localStyles.invoiceButton}
              textStyle={{textTransform: "uppercase"}}/>
          </View>
        }
      </View>
    )
  }

  const reviewModal = () => {
    return(
      <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.3)', padding: 10}}>
        <TouchableOpacity
          style={{flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center'}}
          onPress={() => setShowWriteReviewModal(false)}>
            
            <TouchableWithoutFeedback style={{flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center'}}>
              <View style={localStyles.reviewModalContainer}>
                <TextView style={localStyles.nameText} text={details.customerName}/>
                <TextView style={localStyles.serviceRatingText} text={Constants.serviceRating}/>

                <View style={localStyles.ratingContainer}>
                  <Rating
                      style={localStyles.ratingBar}
                      type="custom"
                      ratingBackgroundColor={color.disable_text_color}
                      ratingColor={color.primaryColor}
                      tintColor='#fff'
                      imageSize={localStyles.ratingStartSize}
                      startingValue={rating}
                      onSwipeRating={(val) => handleRating(val)}
                  />
                  <View style={localStyles.dividerVertical}/>
                  <ImageView style={localStyles.emoji} image={emoji}/>
                </View>
                <TextView style={localStyles.serviceRatingText} text={Constants.wouldYouLikeToAddComment}/>
                <TextInput
                    style={localStyles.commentInput}
                    placeholder={Constants.writeHere}
                    multiline
                    onChangeText={val => setComment(val)}/>
                <Button
                    style={localStyles.invoiceButton}
                    onPress={() => handlePostReview(rating, comment)}
                    text={Constants.send}/>
              </View>
            </TouchableWithoutFeedback>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    )
  }

  const paymentAlertModal = () => {
    return(
      <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.3)', padding: 10}}>
        <TouchableOpacity 
          style={{flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center'}} 
          onPress={() => setShowPaymentAlertModal(false)}>
            <View style={localStyles.paymentModalContainer}>
              <TextView style={localStyles.areYouSure} text={Constants.areYouSure} />
              <TextView style={localStyles.descriptionText} text={Constants.areYouSureDec} />

              <View style={localStyles.paymentModalButtonContainer}>

                <TouchableOpacity 
                  style={localStyles.yesButtonContainer}
                  onPress={() => setShowPaymentAlertModal(false)}>
                  <TextView style={localStyles.yesButtonText} text={Constants.no} />
                </TouchableOpacity>
                
                <View style={{...localStyles.dividerVertical, marginRight: 0, backgroundColor: color.white}}/>

                <TouchableOpacity 
                  style={localStyles.yesButtonContainer}
                  onPress={() => handleReceivedPayment(false)}>
                  <TextView style={localStyles.yesButtonText} text={Constants.yes} />
                </TouchableOpacity>
              </View>
            </View>
        </TouchableOpacity>
      </View>
    )
  }

  return (
    <View style={localStyles.container}>
      <Modal
        visible={showWriteReviewModal}
        style={{margin: 0, flex :1}}
        animationType="fade"
        transparent={true}
        onRequestClose={() => setShowWriteReviewModal(false)}>
          
          {reviewModal()}

      </Modal>

      <Modal
        visible={showPaymentAlertModal}
        style={{margin: 0, flex :1}}
        animationType="fade"
        transparent={true}
        onRequestClose={() => setShowPaymentAlertModal(false)}>

          {paymentAlertModal()}

      </Modal>
      <SafeAreaView>
        <View>
          <HeaderComponent
            showBackButton
            title={Constants.leadDetails}
            onPress={() => {
              params.getBack(shouldUpdate, params.item);
              props.navigation.pop();
            }}
          />
        </View>
        <Divider9 />

        <ScrollView>
          <View style={localStyles.scrollContainer}>
             
            {profileView()}

            <Divider9 />

            {dateView()}

            <Divider9 />

            {details.location == 3 && 
              <View>
                {locationView()}
                <Divider9 />
              </View>
            }

            {details.completed == 1 && 
              <View>
                {jobCompletedView()}
                <Divider9 />
              </View>
            }

            {details.review && 
              <View>
                {reviewView()}
                <Divider9 />
              </View>
            }

            {details.services && 
              <View>
                {servicesView()}
                <Divider9 />
              </View>
            }

            {amountView()}

            <Divider9 />

            {details.jobStartTime && 
              <View>
                {jobStartedView()}
                <Divider9 />
              </View>
            }
            {details.jobEndTime && 
              <View>
                {jobEndedView()}
                <Divider9 />
              </View>
            }

            {details.completed == 1 && details.location == 3 &&  paymentView() }

          </View>
        </ScrollView>
      </SafeAreaView>
      <Progress showProgress={showProgress}/>
    </View>
  );
};
const localStyles = ScaledSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: color.white,
  },
  scrollContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
    paddingBottom: '60@vs',
  },
  profileContainer: {
    flexDirection: "row",
    padding: "12@ms",
    alignItems: 'center'
  },
  profileImage: {
    width: '50@ms',
    height: '50@ms',
    resizeMode: 'contain',
    borderColor: color.light_grey,
    borderRadius: '100@ms',
    borderWidth: '1.5@ms',
  },
  profileTextContainer: {
    flex: 1,
    marginHorizontal: "10@ms"
  },
  profileDateTextContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  profileDateText: {
    fontFamily: 'HeliosAntique-Bold',
    fontSize: "13@ms",
    color: color.account_options,
    textTransform: 'uppercase'
  },
  titleText: {
    fontFamily: 'HeliosAntique-Bold',
    fontSize: "11@ms",
    color: color.black,
    textTransform: 'uppercase'
  },
  descriptionText: {
    fontFamily: "HeliosAntique-Regular",
    fontSize: "12@ms",
    color: color.account_options,
    marginTop: "5@ms"
  },
  viewContainer: {
    padding: "12@ms",
    justifyContent: 'center'
  },
  maps: {
    flex: 1, 
    height: "180@ms",
    marginTop: "10@ms"
  },
  mapButtonContainer: {
    flexDirection: 'row'
  },
  viewMapButton: {
    marginTop: "10@vs",
    height: "25@ms",
    padding: 0,
    justifyContent: 'center'
  },
  callCustomerButton: {
    marginHorizontal: "10@ms",
    backgroundColor: color.light_grey
  },
  invoiceButton: {
    marginTop: "20@vs",
  },
  buttonTextStyle: {
    fontFamily: 'HeliosAntique-Bold',
    fontSize: "9@ms",
    textTransform: "uppercase",
    paddingVertical: 0
  },
  reviewTextContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  paymentTypeContainer: {
    flexDirection: 'row',
    marginTop: "10@ms"
  },
  input: {
    borderColor: color.disable_text_color,
    borderWidth: "1@ms",
    padding: "5@ms",
    fontSize: "13@ms",
    fontFamily: 'Helios Antique',
    marginTop: "5@ms"
  },
  completedContainer: {
    borderRadius: "7@ms",
    backgroundColor: color.completed,
    padding: "10@ms",
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '12@ms'
  },
  completedText: {
    fontFamily: 'HeliosAntique-Bold',
    fontSize: "12@ms",
    textTransform: 'uppercase',
    color: color.start,
    marginHorizontal: '5@ms'
  },
  paymentStatusContainer: {
    borderRadius: "7@ms",
    backgroundColor: color.completed,
    padding: "10@ms",
    flexDirection: 'row',
    marginTop: '12@ms',
    alignItems: 'center'
  },
  paymentStatusText: {
    fontFamily: 'HeliosAntique-Bold',
    fontSize: "10@ms",
    textTransform: 'uppercase',
    color: color.start,
    marginHorizontal: '5@ms'
  },
  writeReviewButton: {
    flex: 1,
    paddingHorizontal: "15@ms",
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color.finish,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderTopEndRadius: "7@ms",
    borderBottomEndRadius: "7@ms"
  },
  writeReviewButtonText: {
    textTransform: 'uppercase',
    fontFamily: 'HeliosAntique-Bold',
    fontSize: "13@ms",
    color: color.white
  },
  startButton: {
    height: "28@ms",
    width: "65@ms",
    justifyContent: 'center',
    backgroundColor: color.completed,
    marginRight: "15@ms"
  },
  startButtonText: {
    fontFamily: 'HeliosAntique-Bold',
      textTransform: "uppercase",
      fontSize: "10@ms",
      color: color.start,
      paddingVertical: 0
  },
  authCodeContainer: {
    marginTop: "15@ms"
  },
  reviewModalContainer: {
    width: '100%',
    backgroundColor: color.white,
    padding: "10@ms",
    borderRadius: "5@ms"
  },
  nameText: {
    fontFamily: 'HeliosAntique-Bold',
    fontSize: "13@ms",
    color: color.black,
    marginTop: "10@ms"
  },
  serviceRatingText: {
    fontFamily: "HeliosAntique-Regular",
    fontSize: "13@ms",
    color: color.black,
    fontWeight: "500",
    marginTop: "15@ms"
  },
  ratingContainer: {
    borderWidth: "0.5@ms",
    borderColor: color.disable_text_color,
    marginTop: "5@ms",
    borderRadius: "5@ms",
    flexDirection: 'row',
    alignItems: 'center'
  },
  ratingBar: {
    flex: 1, 
    padding: "10@ms", 
    alignItems: 'flex-start'
  },
  ratingStartSize: '25@ms',
  dividerVertical: {
    width: "1@ms",
    height: '100%',
    backgroundColor: color.disable_text_color,
    marginRight: "15@ms"
  },
  emoji: {
    width: "35@ms",
    height: "35@ms",
    marginRight: "15@ms"
  },
  commentInput: {
    height: "80@ms",
    borderWidth: "0.5@ms",
    borderColor: color.primaryDarkColor,
    marginTop: "5@ms",
    borderRadius: "5@ms",
    padding: "10@ms",
    textAlign: 'left',
    fontFamily: 'Helios Antuque Regular',
  },
  paymentModalContainer: {
    width: '100%',
    backgroundColor: color.white,
    borderRadius: "10@ms",
    justifyContent: 'center',
    alignItems: 'center'
  },
  areYouSure: {
    fontFamily: 'HeliosAntique-Bold',
    fontSize: "16@ms",
    marginTop: '15@ms'
  },
  paymentModalButtonContainer: {
    width: '100%',
    flexDirection: 'row',
    backgroundColor: color.primaryColor,
    marginTop: "15@ms",
    borderBottomLeftRadius: "10@ms",
    borderBottomRightRadius: "10@ms",
    justifyContent: 'center',
    alignItems: 'center'
  },
  yesButtonContainer: {
    width: '47%', 
    paddingHorizontal: "10@ms", 
    paddingVertical: "12@ms", 
    justifyContent: 'center', 
    alignItems: 'center'
  },
  yesButtonText: {
    fontFamily: 'HeliosAntique-Bold',
    color: color.white,
    fontSize: "15@ms",
    textTransform: 'uppercase'
  }

});

export default LeadDetailsScreen;
