import React, {useState, useEffect} from 'react';
import {View, FlatList} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import { ScaledSheet } from 'react-native-size-matters';

import {leadsRequest, leadDetailsRequest} from '../api/HandleRequest';
import UserModal from '../modal/UserModal';
import LeadMoal from '../modal/LeadModal';
import color from '../resources/color';

import Divider9 from '../components/Divider9';
import HeaderComponent from '../components/HeaderComponent';
import LeadSelectionComponent from '../components/LeadSelectionComponent';
import LeadComponent from '../components/LeadComponent';

import Progress from '../components/Progress';
import Constants from '../common/Constants';
import moment from 'moment';
import NoLeadComponent from '../components/NoLeadComponent';
import { useDispatch, useSelector } from 'react-redux';
import { getUserData } from '../store/AppReducer';

const NewLeadsScreen = props => {
  
  const propData = props.route.params;
  const dispatch = useDispatch()
  const userData = new UserModal(useSelector(state => getUserData(state)));

  const [leadSelection, setLeadSelection] = useState(propData.newLead ? Constants.newLeadSelection : Constants.ongoingSelection);
  const [data, setData] = useState([]);
  const [showProgress, setShowProgress] = useState(false);
  const [selectedLead, setSelectedLead] = useState('');
  const currentDate = moment(Date.now()).format('YYYY-MM-DD HH:mm');

  useEffect(() => {
    handleSelectedLead(userData, leadSelection[0])
  }, []);

  const handleSelectedLead = async (userModal, selectedLead) => {
    setShowProgress(true)
    setSelectedLead(selectedLead);

    try{
      const payload = {staffid: userModal.id, type: propData.newLead ? "NEW" : "OLD", period: selectedLead.toUpperCase()};
      const responseModal = await leadsRequest({payload: payload});
      const body = responseModal.body;
      if(body.status?.toUpperCase() === "FAILED"){
        setData([]);
      }else{
        const data = body?.details || [];
        let tempData = [];
        for(let i=0; i<data.length; i++){
          tempData.push(new LeadMoal(currentDate, data[i]));
        }
        setData(tempData);
      }
    }catch(e){
      setData([]);
     }
    setShowProgress(false)
  }

  const getBack = (flag, item) => {
    dispatch({ type: 'updateData', updateData: true })//hardcode
    if(flag){
      dispatch({ type: 'updateData', updateData: flag })
      handleSelectedLead(userData, selectedLead)
    } 
  }

  const handleLeadDetails = async (userModal, item) => {
    setShowProgress(true)
    try{
      const payload = {staffid: userModal.id, refid: item.refId};
      const responseModal = await leadDetailsRequest({payload: payload});
      if(responseModal.status === "FAILED"){
        alert(responseModal.message)
      }else{
        props.navigation.navigate('LeadDetails', {data: responseModal.body, item: item, getBack: getBack});
      }
    }catch(e){ 
      alert(Constants.unexpectedError)
    }
    setShowProgress(false)
    
  }

  return (
    <View style={localStyles.container}>
      <SafeAreaView>
        <HeaderComponent
          title={propData.newLead ? Constants.newLeads : Constants.ongoingLeads}
          onPress={() => {
            props.navigation.pop();
          }}
        />
        <Divider9 />
        <View>
          <FlatList 
              horizontal
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={localStyles.yearList}
              keyExtractor={(item, index) => {return index.toString()}}
              data={leadSelection}
              renderItem={({item}) => 
                  <LeadSelectionComponent 
                      selectedLead={selectedLead}
                      text={item} 
                      onPress={() => handleSelectedLead(userData, item)}
                  />
              }
          />
          <Divider9 />
          {data.length === 0 && <NoLeadComponent />}
          <FlatList 
              contentContainerStyle={localStyles.extraPadding}
              keyExtractor={(item, index) => {return index.toString()}}
              data={data}
              renderItem={({item}) => 
                  <LeadComponent 
                      data={item}
                      onPress={() => handleLeadDetails(userData, item)}
                  />
              }
          />
        </View>
      </SafeAreaView>
      <Progress showProgress={showProgress}/>
    </View>
  );
};

const localStyles = ScaledSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: color.white
  },
  yearList: {
    margin: "10@ms",
    paddingEnd: "20@ms"
  },
  extraPadding: {
    marginTop: "8@ms",
    paddingBottom: "280@vs"
  }
})

export default NewLeadsScreen;
