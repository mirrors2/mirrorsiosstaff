import React, {useState, useEffect} from 'react';
import {ScrollView, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {SafeAreaView} from 'react-native-safe-area-context';

import color from '../resources/color';
import Constants from '../common/Constants';
import HeaderComponent from '../components/HeaderComponent';
import ImageView from '../utils/ImageView';
import TextView from '../utils/TextView';
import Divider9 from '../components/Divider9';

const ProfileScreen = props => {
  const userModal = props.route?.params?.userData
  const [imageError, setImageError] = useState(false);

  return (
    <View style={localStyles.container}>
      <SafeAreaView>
        <View>
          <HeaderComponent
            showBackButton
            title={Constants.profile}
            description={Constants.accountDec}
            onPress={() => { props.navigation.pop()}}/>
          <ImageView
            style={localStyles.profileImage}
            resizeMode={'contain'}
            image={ imageError ? require('../resources/drawable/profile.png') : {uri: userModal.image} }
            onError={err => setImageError(true)}/>
        </View>

        <Divider9 />

        <ScrollView>
          <View style={localStyles.scrollContainer}>
            <TextView
              style={localStyles.headerText}
              text={Constants.basicDetails}
            />
            <View style={localStyles.itemContainer}>
              <TextView style={localStyles.itemText} text={Constants.name} />
              <TextView style={localStyles.valueText} text={userModal.name} />
            </View>
            <View style={localStyles.itemContainer}>
              <TextView style={localStyles.itemText} text={Constants.dob} />
              <TextView style={localStyles.valueText} text={userModal.dob} />
            </View>
            <View style={localStyles.itemContainer}>
              <TextView style={localStyles.itemText} text={Constants.email} />
              <TextView style={localStyles.valueText} text={userModal.email} />
            </View>
            <View style={localStyles.itemContainer}>
              <TextView style={localStyles.itemText} text={Constants.contact} />
              <TextView
                style={localStyles.valueText}
                text={userModal.contact}
              />
            </View>
            <View style={localStyles.itemContainer}>
              <TextView
                style={localStyles.itemText}
                text={Constants.designation}
              />
              <TextView
                style={localStyles.valueText}
                text={userModal.specialist}
              />
            </View>
            <View style={localStyles.itemContainer}>
              <TextView
                style={localStyles.itemText}
                text={Constants.startDate}
              />
              <TextView
                style={localStyles.valueText}
                text={userModal.startDate}
              />
            </View>
            <View style={localStyles.itemContainer}>
              <TextView style={localStyles.itemText} text={Constants.endDate} />
              <TextView
                style={localStyles.valueText}
                text={userModal.endDate}
              />
            </View>

            <Divider9 />

            <TextView
              style={localStyles.headerText}
              text={Constants.location}
            />
            <View style={localStyles.itemContainer}>
              <TextView
                style={localStyles.itemText}
                text={Constants.primaryLocation}
              />
              <TextView
                style={localStyles.valueText}
                text={userModal.location}
              />
            </View>

            <Divider9 />

            <TextView
              style={localStyles.headerText}
              text={Constants.category}
            />
            <View style={localStyles.itemContainer}>
              <TextView
                style={localStyles.itemText}
                text={Constants.primaryCategory}
              />
              <TextView
                style={localStyles.valueText}
                text={userModal.primaryCategory}
              />
            </View>
            <View style={localStyles.itemContainer}>
              <TextView
                style={localStyles.itemText}
                text={Constants.secondaryCategory}
              />
              <TextView
                style={localStyles.valueText}
                text={userModal.secondaryCategory}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

const localStyles = ScaledSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: color.white,
  },
  scrollContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
    paddingBottom: '60@vs',
  },
  profileImage: {
    width: '50@ms',
    height: '50@ms',
    resizeMode: 'contain',
    borderColor: color.light_grey,
    borderRadius: '100@ms',
    borderWidth: '1.5@ms',
    position: 'absolute',
    right: '10@ms',
  },
  headerText: {
    flex: 1,
    fontFamily: 'HeliosAntique-Bold',
    fontSize: '18@ms',
    marginStart: '14@ms',
    marginTop: '8@ms',
    color: color.black,
  },
  itemContainer: {
    borderBottomWidth: '1.5@vs',
    borderBottomColor: '#f5f5f5',
    padding: '10@ms',
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemText: {
    flex: 1,
    fontFamily: "HeliosAntique-Regular",
    fontSize: '12@ms',
    marginHorizontal: '5@ms',
    marginVertical: '2@ms',
    color: color.black,
    textTransform: 'uppercase',
  },
  valueText: {
    flex: 1,
    fontFamily: "HeliosAntique-Regular",
    fontSize: '12@ms',
    marginHorizontal: '5@ms',
    marginVertical: '2@ms',
    color: color.account_options,
    textAlign: 'right',
  },
  arrowIconSize: '20@ms',
});

export default ProfileScreen;
