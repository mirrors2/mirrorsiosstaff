import React, {useState} from 'react';
import {View, TextInput, ScrollView, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/Ionicons';

import {loginRequest} from '../api/HandleRequest';
import ForegroundCurve from '../components/ForegroundCurve'
import TextView from '../utils/TextView';
import ImageView from '../utils/ImageView';
import Button from '../components/Button';

import colors from '../resources/color';
import {save} from '../utils/SharedPreferences';
import PreferenceKeys from '../common/PreferenceKeys';
import Constants from '../common/Constants';
import { useDispatch } from 'react-redux';
import Progress from '../components/Progress';

const LoginScreen = props => {

    const dispatch = useDispatch();
    const [emailId, setEmailId] = useState('');
    const [password, setPassword] = useState('');
    const [emailError, setEmailError] = useState("Email address cannot be blank");
    const [passwordError, setPasswordError] = useState("Password must be 6-15 characters");
    const [showEmailError, setShowEmailError] = useState(true);
    const [showPasswordError, setShowPasswordError] = useState(true);
    const [showProgress, setShowProgress] = useState(false);

    const handleEmailId = (input) => {
        let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (input.match(regexEmail)) {
            setEmailId(input);
            setShowEmailError(false); 
        } else if(input === ''){
            setEmailError("Email address cannot be blank")
            setShowEmailError(true);
        } else {
            setEmailError("Invalid email address")
            setShowEmailError(true);
        }
    }

    const handlePassword = (input) => {
        let regexPassword =  /^[a-zA-Z0-9!@#$%^&*]{6,16}$/;
        if (input.match(regexPassword)) {
            setPassword(input);
            setShowPasswordError(false); 
        } else {
            setPasswordError("Password must be 6-15 characters")
            setShowPasswordError(true);
        }
    }

    const loginHandle = async () => {
        if(showEmailError) return alert(emailError);
        if(showPasswordError) return alert(passwordError);

        setShowProgress(true)
        const payload = {emailid: emailId, password: password};
        const responseModal = await loginRequest({payload: payload});
        if(responseModal.status === "FAILED"){
            alert(responseModal.message)
        }else{
            const userData = responseModal.body;
            dispatch({type: 'userData', userData})
            await save(PreferenceKeys.userData, JSON.stringify(userData));
            props.navigation.replace("Tabs")
        }
        setShowProgress(false)
    }

  return (
    <View style={localStyles.container}>
        <View style={localStyles.logoContainer}>
            <ImageView 
                style={localStyles.logo}
                resizeMode={'contain'}
                image={require('../resources/drawable/app_logo.png')} />
        </View>
        <ForegroundCurve style={localStyles.foregroundContainer}>
            <ScrollView>
                <View style={{paddingBottom: 250}}>
                    <TextView 
                        style={localStyles.headerText}
                        text={'Staff Login'}/>
                    <TextView 
                        style={localStyles.placeholderText}
                        text={'Enter email address'}/>
                    <TextInput
                        style={localStyles.input}
                        onChangeText={input => handleEmailId(input)}
                        keyboardType='email-address'
                        autoCapitalize = 'none'/>
                    {showEmailError && 
                        <View style={localStyles.errorTextContainer}>
                            <Icon name='ios-warning-outline' color={colors.primaryColor} size={localStyles.errorIcon}/>
                            <TextView 
                                style={localStyles.errorText}
                                text={emailError}/>
                        </View>
                    }

                    <TextView 
                        style={localStyles.placeholderText}
                        text={'Enter Password'}/>
                    <TextInput 
                        style={localStyles.input}
                        onChangeText={input => handlePassword(input)}
                        secureTextEntry={true}
                        keyboardType='default'
                        autoCapitalize = 'none'
                    />
                    {showPasswordError && 
                        <View style={localStyles.errorTextContainer}>
                            <Icon name='ios-warning-outline' color={colors.primaryColor} size={localStyles.errorIcon}/>
                            <TextView 
                                style={localStyles.errorText}
                                text={passwordError}/>
                        </View>
                    }

                    <Button
                        text={Constants.login}
                        onPress={loginHandle}
                        style={localStyles.button}
                        textStyle={{textTransform: "uppercase"}}/>
                </View>
            </ScrollView>
        </ForegroundCurve>
        <Progress showProgress={showProgress} />
    </View>
  );
};

const localStyles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.login_background
  },
  logoContainer: {
      flex: 0.35,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: "10@vs"
  },
  logo: {
      width: '70%',
  },
  foregroundContainer: {
    flex: 0.65,
  },
  headerText: {
    width: '100%',
    textAlign: 'center',
    fontSize: "30@ms",
    fontFamily: 'Helios Antique',
    fontWeight: 'bold'
  },
  placeholderText: {
    width: '100%',
    textAlign: 'left',
    fontSize: "11@ms",
    fontFamily: 'Helios Antique',
    marginTop: "25@ms"
  },
  input: {
    borderBottomColor: colors.primaryColor,
    borderBottomWidth: "1@ms",
    marginTop: "8@ms",
    padding: "5@ms",
    fontSize: "13@ms",
    fontFamily: 'Helios Antique',
  },
  errorTextContainer: {
    flexDirection: 'row',
    marginTop: "5@ms",
    alignItems: 'center'
  },
  errorText: {
    width: '100%',
    textAlign: 'left',
    fontSize: "11@ms",
    fontFamily: 'Helios Antique',
    color: colors.primaryColor,
    marginLeft: "5@ms"
  },
  errorIcon: "15@ms",
  button: {
      marginTop: "50@ms"
  }
});

export default LoginScreen;
