import React from 'react';
import {Text, StyleSheet} from 'react-native';

const style = StyleSheet.create({
    text: {
        color: 'black',
    }
});

export default (props) => {
    return <Text {...props} style={{...style.text, ...props.style}} onPress={props.onPress}>{props.text} {props.children}</Text>
}
