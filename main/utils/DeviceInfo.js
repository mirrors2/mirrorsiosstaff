import {Platform} from 'react-native';
import DeviceInfo from 'react-native-device-info';

export const deviceId = async () => {
    if(Platform.OS === 'android'){
        return await DeviceInfo.getMacAddress();
    }else{
       return DeviceInfo.getUniqueId();
    }
};
