import React from 'react';
import {Platform, TouchableNativeFeedback, TouchableWithoutFeedback, TouchableOpacity} from 'react-native';

// const TouchableView = (
//     (Platform.OS === 'android' && Platform.Version >= 21)
//         ? TouchableNativeFeedback
//         : TouchableOpacity
// );

export const TouchableViewWithFeedback = (props) => {
    if(Platform.OS === 'android' && Platform.Version >= 21){
        return <TouchableNativeFeedback style={props.style} onPress={props.onPress}>{props.children}</TouchableNativeFeedback>
    }
    return (<TouchableOpacity style={props.style} onPress={props.onPress}>{props.children}</TouchableOpacity>)
};

export const TouchableView = (props) => {
    return (<TouchableOpacity style={props.style} onPress={props.onPress}>{props.children}</TouchableOpacity>)
};

export const TouchableViewWithoutFeedback = (props) => {
    return (<TouchableWithoutFeedback style={props.style}> {props.children}</TouchableWithoutFeedback>)
};
