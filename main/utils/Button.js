import React from 'react';
import {View, Text, StyleSheet, TouchableNativeFeedback, TouchableOpacity} from 'react-native';

import * as Platform from '../utils/Platform';

const styles = StyleSheet.create({
    buttonContainer: {
        padding: 10,
        alignItems: 'center',
        backgroundColor: 'blue'
    },
    text: {
        textAlign: 'center',
        color: 'white',
        fontSize: 15
    }
});

export default (props) => {

    if(Platform.isAndroid && Platform.version >= 21){
        return(
            <TouchableNativeFeedback style={{width: '100%'}} onPress={props.onPress}>
                <View style={{...styles.buttonContainer, ...props.buttonStyle}} >
                    <Text style={{...styles.text, ...props.textStyle}}>{props.text}</Text>
                </View>
            </TouchableNativeFeedback>
        )
    }
    return(
        <TouchableOpacity style={{width: '100%' }} onPress={props.onPress}>
            <View style={{...styles.buttonContainer, ...props.buttonStyle}} >
                <Text style={{...styles.text, ...props.textStyle}}>{props.text}</Text>
            </View>
        </TouchableOpacity>
    )

    // const TouchableView = (Platform.isAndroid && Platform.version >= 21) ? TouchableNativeFeedback : TouchableOpacity;
    // return (
        // <View style={{...styles.buttonContainer, ...props.style}} >
        //     <TouchableView style={{width: '100%' }} onPress={props.onPress}>
        //         <View >
        //             <Text style={{...styles.text, ...props.style}}>{props.text}</Text>
        //         </View>
        //     </TouchableView>
        // </View>
    // )
}
