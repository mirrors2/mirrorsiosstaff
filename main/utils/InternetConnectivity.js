import NetInfo from '@react-native-community/netinfo';
//
const InternetConnectivity = async () => {
    const state = await NetInfo.fetch();
    return {connectionType: state.type, isConnected: state.isConnected}
};

// const InternetConnectivity = async () => {
//     try {
//         // For Android devices
//         if (Platform.OS === "android") {
//             return await NetInfo.isConnected.fetch();
//         } else {
//             // For iOS devices
//             return await NetInfo.isConnected.addEventListener("connectionChange", handleFirstConnectivityChange);
//         }
//     }catch (error) {
//         return false;
//     }
// };
// const handleFirstConnectivityChange = async isConnected => {
//     NetInfo.isConnected.removeEventListener(
//         "connectionChange",
//         handleFirstConnectivityChange
//     );
//     return isConnected;
// };

export default InternetConnectivity;
