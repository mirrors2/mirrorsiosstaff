import React from 'react';
import {Image, StyleSheet} from 'react-native';

const style = StyleSheet.create({
    image: {
        width: '50%',
        height: '50%'
    }
});

//{uri: 'https://5.imimg.com/data5/GX/JR/MY-25550583/mineral-water-cans-500x500.jpg'};
//require('../../../res/drawable/can_20.jpg');


export default (props) => {
    const image =  props.image;
    return <Image
        {...props}
        style ={{...style.image, ...props.style}}
        source={image}
        />
}
