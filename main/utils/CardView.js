import React from 'react';
import {View, StyleSheet} from 'react-native';

const style = StyleSheet.create({
    cardContainer: {
        elevation: 2,
        shadowColor: 'black',
        shadowOpacity: 0.5,
        shadowOffset: {width: 0, height: 1},
        padding: 10,
        shadowRadius: 2,
        borderRadius: 5,
        backgroundColor: 'white',
    }
});

export default (props)=> {
    return <View style={{...style.cardContainer, ...props.style}}>{props.children}</View>
}
