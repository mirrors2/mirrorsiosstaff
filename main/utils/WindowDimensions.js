import {Dimensions} from 'react-native';

export default function WindowDimensions (){
    const dimensions = Dimensions.get('window');
    return {width: dimensions.width, height: dimensions.height}
}