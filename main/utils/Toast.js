import {ToastAndroid, Platform, Alert} from "react-native";

export default (props) => {
    if (props.visible) {
        if(props.message === undefined){
            return null;
        }
        if(Platform.OS === "android"){
            ToastAndroid.show(props.message, ToastAndroid.LONG);
        }else{
            Alert.alert(props.message, '',[{text: 'Ok', onPress: () => null}])
        }
        return null;
    }
    return null;
};