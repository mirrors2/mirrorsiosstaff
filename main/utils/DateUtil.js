import moment from "moment";

export const getDate = (now, date, time) => {
    if(date === '') return "--"

    let then = date + " " + time;
    try{
        var isSame = moment(now, "YYYY-MM-DD").isSame(moment(then, "YYYY-MM-DD"));
        if(isSame) return "TODAY";

        var isBefore = moment(now, "YYYY-MM-DD H:mm").isBefore(moment(then, "YYYY-MM-DD HH:mm"));
        if(isBefore) {
            const diff = moment(now, "YYYY-MM-DD").diff(moment(then, "YYYY-MM-DD"))
            const hours = moment.duration(diff).asHours();
            return hours <= 24 ? "TOMORROW" : date;
        }

        var isAfter = moment(now, "YYYY-MM-DD HH:mm").isAfter(moment(then, "YYYY-MM-DD HH:mm"));
        if(isAfter) {
            const diff = moment(now, "YYYY-MM-DD").diff(moment(then, "YYYY-MM-DD"))
            const hours = moment.duration(diff).asHours();
            return hours <= 24 ? "YESTERDAY" : date;
        }
    }catch(e){console.log('error', e)}
    return date;
}