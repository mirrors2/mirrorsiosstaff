import React, {useState} from 'react';
import {TouchableOpacity, View} from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/Ionicons';

import TextView from '../utils/TextView';

import colors from '../resources/color';

const HeaderComponent = (props) => {

    return(
        <View style={localStyles.container}>
            <View style={localStyles.titleContainer}>
                {props.showBackButton &&
                    <TouchableOpacity style={localStyles.iconContainer} onPress={props.onPress}>
                        <Icon name='md-arrow-back' size={localStyles.backIcon} color={colors.black}/>
                    </TouchableOpacity>
                }
                {props.title && <TextView style={localStyles.title} text={props.title}/>}
            </View>
            {props.description && <TextView style={localStyles.description} text={props.description}/>}
        </View>
    )
}

const localStyles = ScaledSheet.create({
    container: {
        width: '100%',
        padding: "5@ms"
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    iconContainer: {
        marginStart: "5@ms"
    },
    backIcon: "28@ms",
    title: {
        marginHorizontal: "5@ms",
        fontFamily: 'HeliosAntique-Bold',
        fontSize: "23@ms"
    },
    description: {
        marginHorizontal: "5@ms",
        fontFamily: "HeliosAntique-Regular",
        fontWeight: '500',
        fontSize: "14@ms",
        marginVertical: "5@ms"
    }
});

export default HeaderComponent;