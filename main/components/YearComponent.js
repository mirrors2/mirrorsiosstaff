import React from "react";
import {TouchableOpacity, View} from 'react-native';
import { ScaledSheet } from "react-native-size-matters";

import color from '../resources/color';
import TextView from '../utils/TextView';

const YearComponent = (props) => {

    const isSelected = props.selectedYear === props.text;
    return(
        <TouchableOpacity onPress={() => props.onPress()}>
            <View style={isSelected ? localStyles.container : {...localStyles.container, ...localStyles.disableStyle}}>
                <TextView style={{...localStyles.text, color: isSelected ? color.white : color.black}} text={props.text}/>
            </View>
        </TouchableOpacity>
    )
}

const localStyles = ScaledSheet.create({
    container: {
        backgroundColor: color.primaryColor,
        borderRadius: "5@ms",
        marginRight: "12@ms",
        paddingHorizontal: "12@ms",
        paddingVertical: "5@vs"
    },
    disableStyle: {
        backgroundColor: color.light_grey,
    },
    text: {
        fontFamily: 'HeliosAntique-Bold',
        fontSize: "12@ms",
    }
    
});

export default YearComponent;