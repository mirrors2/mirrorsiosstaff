import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import color from '../resources/color';
import {ScaledSheet} from 'react-native-size-matters';
import Constants from '../common/Constants';
import TextView from '../utils/TextView';

class BottomTabIcon extends Component {

    constructor(props){
        super(props);
        this.count = props.count;
    }
  render() {
    return (
      <View style={localStyles.container}>
        <Icon
          name={
            this.props.type === Constants.newLeads
              ? 'ios-repeat-outline'
              : 'ios-list-outline'
          }
          color={this.props.color}
          size={localStyles.iconSize} />

        {this.count > 0 && (
          <View style={localStyles.roundContainer}>
            <TextView style={localStyles.text} text={this.props.count} />
          </View>
        )}
      </View>
    );
  }
}


const localStyles = ScaledSheet.create({
  container: {
    width: '24@ms',
    height: '24@ms',
    margin: '1@ms',
  },
  roundContainer: {
    position: 'absolute',
    right: '-5@ms',
    top: '-5@ms',
    backgroundColor: 'red',
    padding: '3@ms',
    borderRadius: '100@ms',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconSize: '26@ms',
  text: {
    color: color.white,
    fontSize: '12@ms',
    textAlign: 'center',
  },
});
export default BottomTabIcon;
