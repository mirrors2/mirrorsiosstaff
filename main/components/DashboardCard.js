import React from 'react';
import {View} from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/Ionicons';

import CardView from '../utils/CardView';
import color from '../resources/color';
import TextView from '../utils/TextView';


const DashboardCard = props => {
  return (
    <CardView style={{...localStyles.card, ...props.style}}>
      <View style={{flex: 1}}>
        <TextView style={localStyles.countText} text={props.count} />
        <TextView style={localStyles.labelText} text={props.lable} />
      </View>
      <View style={{...localStyles.roundContainer, backgroundColor: props.circleColor}}>
        <Icon name={props.iconName} color={props.iconColor} size={localStyles.iconSize} />
      </View>
    </CardView>
  );
};

const localStyles = ScaledSheet.create({
    card: {
      marginHorizontal: "5@ms",
      flex: 0.5,
      flexDirection: 'row',
      alignItems: 'center'
    },
    cardRight: {
      marginStart: "10@ms",
    },
    countText: {
        fontFamily: 'HeliosAntique-Bold',
      fontSize: "22@ms",
      marginVertical: "5@vs"
    },
    labelText: {
        fontFamily: "HeliosAntique-Regular",
      fontSize: "13@ms",
      fontWeight: "500",
      color: "#000",
      marginBottom: "10@ms"
    },
    roundContainer: {
      borderRadius: "100@ms", 
      padding: "8@ms", 
      backgroundColor: color.account_options
    },
    iconSize: "23@ms",
  })

  export default DashboardCard;