import React from "react";
import { View } from "react-native";
import { ScaledSheet } from "react-native-size-matters";
import Constants from "../common/Constants";
import color from "../resources/color";
import TextView from "../utils/TextView";
import Button from "./Button";

const CustomerServiceComponent = (props) => {
    const data = props.data
    const isStarted = data.serviceStartTime;
    const isFinished = isStarted && data.serviceFinishTime;

    return (
        <View style={localStyles.container}>
            <View style={localStyles.textContainer}>
                <View style={localStyles.dot}/>
                <View>
                    <TextView style={localStyles.text} text={data.serviceName}/>
                    <TextView style={localStyles.qtyText} text={`${Constants.quantity} : ${data.quantity}`}/>
                </View>
            </View>
            {props.showButtons && 
                <View style={localStyles.buttonContainer}>
                <Button 
                    text={data.serviceStartTime || Constants.start}
                    style={{...localStyles.startButton, backgroundColor: isStarted ? color.start : color.completed}}
                    textStyle={{...localStyles.startButtonText, color: isStarted ? color.white : color.start}}
                    onPress={() => isFinished ? alert(Constants.alreadyFinished) : isStarted ? alert(Constants.alreadyStarted) : props.startOnPress()}
                />
                <Button 
                    text={data.serviceFinishTime || Constants.finish}
                    style={{...localStyles.startButton, ...localStyles.finishButton, backgroundColor: isFinished ? color.finish : color.primaryLightColor}}
                    textStyle={{...localStyles.startButtonText, ...localStyles.finishButtonText, color: isFinished ? color.white : color.finish}}
                    onPress={() => isFinished ? alert(Constants.alreadyFinished) : props.finishOnPress()}
                />
            </View>
            }
        </View>
    )
};

const localStyles = ScaledSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: "10@ms"
    },
    textContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    dot: {
        width: "5@ms",
        height: "5@ms",
        backgroundColor: color.disable_text_color,
        borderRadius: "100@ms"
    },
    text: {
        fontFamily: "HeliosAntique-Regular",
        marginHorizontal: "10@ms",
        fontSize: "12@ms",
    },
    qtyText: {
        fontFamily: "HeliosAntique-Regular",
        marginHorizontal: "10@ms",
        fontSize: "10@ms",
    },
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    startButton: {
        height: "30@ms",
        width: "70@ms",
        justifyContent: 'center',
        backgroundColor: color.completed
    },
    startButtonText: {
        fontFamily: 'HeliosAntique-Bold',
        textTransform: "uppercase",
        fontSize: "10@ms",
        color: color.start,
        paddingVertical: 0
    },
    finishButton: {
        backgroundColor: color.primaryLightColor,
        marginLeft: "5@ms"
    },
    finishButtonText: {
        color: color.finish,
    },
});

export default CustomerServiceComponent;