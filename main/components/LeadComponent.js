import React from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Constants from '../common/Constants';

import color from '../resources/color';
import TextView from '../utils/TextView';

const LeadComponent = props => {
  
  const data = props.data;
  const status = data.type
  return (
    <TouchableOpacity onPress={() => props.onPress()}>
      <View style={localStyles.container}>
        <View style={localStyles.leftContainer}>
          
          <TextView style={localStyles.nameText} text={data.name}/>
          <TextView style={localStyles.serviceNameText} text={data.serviceName}/>

          <View style={{...localStyles.leadTextContainer, backgroundColor: color.statusColor[status]}}>
            <Text style={localStyles.leadStatusText}>{status === "Cancelled" ? Constants.cancelledByCustomer : status}</Text>
          </View>
        </View>
        <View style={localStyles.rightContainer}>
          <TextView style={localStyles.dateText} text={data.date}/>
          <TextView style={localStyles.timeText} text={data.time}/>
        </View>
      </View>
    </TouchableOpacity>
  );
};


const localStyles = ScaledSheet.create({
  container: {
    flex: 1,
    padding: "10@ms",
    marginHorizontal: "10@ms",
    marginVertical: "6@ms",
    borderWidth: "0.5@ms",
    borderColor: color.lead_disable_gray_color,
    borderRadius: "5@ms",
    flexDirection: 'row',
    alignItems: 'center'
  },
  leftContainer: {
    flex: 1.35,
    justifyContent: 'center',
  },
  rightContainer: {
    flex: 0.65,
    justifyContent: 'center',
    alignItems: 'center'
  },
  nameText: {
    fontFamily: 'HeliosAntique-SemiBold',
    fontSize: "13@ms",
    color: color.black,
  },
  serviceNameText: {
    fontFamily: "HeliosAntique-Regular",
    fontSize: "13@ms",
    color: color.account_options,
    textTransform: 'uppercase',
    marginTop: "3@ms"
  },
  leadTextContainer: {
    marginTop: "5@ms",
    backgroundColor: color.ongoing_status,
    alignSelf:'baseline',
    borderRadius: "50@ms",
    paddingHorizontal: "12@ms",
    paddingVertical: "2.5@ms"
  },
  leadStatusText: {
    fontFamily: 'HeliosAntique-SemiBold',
    fontSize: "11@ms",
    color: color.white
  },
  dateText: {
    fontFamily: "HeliosAntique-Regular",
    fontSize: "14@ms",
    fontWeight: '500',
    color: color.account_options,
    textTransform: 'uppercase'
  },
  timeText: {
    fontFamily: 'HeliosAntique-Black',
    fontSize: "14@ms",
    fontWeight: '500',
    color: color.black,
    marginTop: "3@ms",
    textTransform: 'uppercase'
  },
  
});

export default LeadComponent;
