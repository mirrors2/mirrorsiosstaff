import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Constants from '../common/Constants';
import color from '../resources/color';
import TextView from '../utils/TextView';

const Progress = props => {
  return (
    <View style={{...localStyles.container, backgroundColor: '#00000000', height: props.showProgress ? localStyles.container.height : 0}}>
      {props.showProgress && (
        <View style={localStyles.container}>
          <View style={localStyles.indicatorContainer}>
            <ActivityIndicator color={color.primaryDarkColor} />
            <TextView style={localStyles.text} text={Constants.loading} />
          </View>
        </View>
      )}
    </View>
  );
};

const localStyles = ScaledSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(171, 171, 171, 0.5)',
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
  },
  indicatorContainer: {
    backgroundColor: color.white,
    borderRadius: '5@ms',
    paddingHorizontal: '20@ms',
    paddingVertical: '5@ms',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  text: {
    marginHorizontal: '10@ms',
    fontFamily: 'Helios Antique',
    color: color.black,
    fontWeight: '500',
    fontSize: '15@ms',
  },
});

export default Progress;
