import React from 'react';
import {TouchableOpacity, TouchableWithoutFeedback, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/Ionicons';
import Constants from '../common/Constants';

import color from '../resources/color';
import TextView from '../utils/TextView';
import Divider9 from './Divider9';

const CSSPTComponent = props => {

  const targetPointView = (targetPoint) => {
    return(
      <View style={localStyles.targetPointContainer}>
          <View style={{...localStyles.targetPointCircle, backgroundColor: targetPoint >= 1 ? color.targetAchieved : color.disable_text_color}}/>
          <View style={{...localStyles.targetPointLine, backgroundColor: targetPoint >= 2 ? color.targetAchieved : color.disable_text_color}}/>
          <View style={{...localStyles.targetPointCircle, backgroundColor: targetPoint >= 2 ? color.targetAchieved : color.disable_text_color}}/>
          <View style={{...localStyles.targetPointLine, backgroundColor: targetPoint >= 3 ? color.targetAchieved : color.disable_text_color}}/>
          <View style={{...localStyles.targetPointCircle, backgroundColor: targetPoint >= 3 ? color.targetAchieved : color.disable_text_color}}/>
      </View>
    )
  }

  const itemView = () => (
    <View style={localStyles.container}>
      <View style={localStyles.itemContainer}>
        <View style={localStyles.roundContainer}>
          <Icon
            name={props.iconName}
            color={color.account_options}
            size={localStyles.iconSize}
          />
        </View>

        <View
          style={{ flex: 1, flexDirection: props.type === Constants.target ? 'column' : 'row', }}>
          <TextView style={localStyles.monthYear} text={props.data.monthYear} />
          <TextView style={localStyles.priceText} text={props.data.totalPrice}/>
        </View>
        {props.type === Constants.salary && props.data.detail === 'Y' && (
          <Icon
            name="chevron-forward-outline"
            size={localStyles.arrowIconSize}
            color={color.edit_account}
          />
        )}
        {props.type === Constants.target && (
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            {targetPointView(props.data.targetPoint)}
            <Icon
              name="chevron-forward-outline"
              size={localStyles.arrowIconSize}
              color={color.edit_account}
            />
          </View>
        )}
      </View>
      <Divider9 />
    </View>
  );
  return (
    <View>
      {(props.type === Constants.salary && props.data.detail === 'Y') || props.type === Constants.target ? 
        ( <TouchableOpacity onPress={() => props.onPress()}>{itemView()}</TouchableOpacity>) : ( itemView() )}
    </View>
  );
};

const localStyles = ScaledSheet.create({
  container: {
    width: '100%',
  },
  itemContainer: {
    flexDirection: 'row',
    padding: '10@ms',
    alignItems: 'center',
  },
  roundContainer: {
    borderRadius: '100@ms',
    padding: '10@ms',
    backgroundColor: color.light_grey,
  },
  iconSize: '23@ms',
  monthYear: {
    flex: 1,
    fontFamily: "HeliosAntique-Regular",
    fontSize: '13@ms',
    marginHorizontal: '10@ms',
  },
  priceText: {
    fontFamily: 'HeliosAntique-Bold',
    fontSize: '13@ms',
    marginHorizontal: '10@ms',
  },
  arrowIconSize: '20@ms',
  targetPointContainer: {
    flexDirection: 'row', 
    alignItems: 'center', 
    marginHorizontal: "10@ms"
  },
  targetPointCircle: {
    width: "10@ms", 
    height: "10@ms",
    borderRadius: "100@ms"
  },
  targetPointLine: {
    width: "8@ms", 
    height: "1@ms", 
  }
});

export default CSSPTComponent;
