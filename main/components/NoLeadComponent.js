import React from "react";
import { View } from "react-native";
import { ScaledSheet } from "react-native-size-matters";

import color from "../resources/color";
import TextView from "../utils/TextView";
import Constants from "../common/Constants";

const NoLeadComponent = (props) => {
    return(
        <View style={localStyles.container}>
            <TextView style={localStyles.noLeadText} text={Constants.noMoreLeads}/>
            <TextView style={localStyles.noLeadDecText} text={Constants.noMoreLeadsDec}/>
        </View>
    )
};

const localStyles = ScaledSheet.create({
    container: {
        paddingVertical: "20@ms",
        paddingHorizontal: "15@vs",
        margin: "10@ms",
        borderWidth: "0.4@ms",
        borderRadius: "5@ms",
        borderColor: color.account_options,
        justifyContent: 'center',
        alignItems: 'center'
    },
    noLeadText: {
        fontFamily: 'HeliosAntique-Bold',
        fontSize: "14@ms",
    },
    noLeadDecText: {
        fontFamily: "HeliosAntique-Regular",
        fontSize: "12@ms",
        color: color.account_options,
        marginTop: "6@vs"
    }
});

export default NoLeadComponent;