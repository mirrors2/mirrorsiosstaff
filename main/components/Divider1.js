import React from 'react';
import {View} from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';

const Divider1 = (props) => {
    return(<View style={localStyles.container}/>)
}

const localStyles = ScaledSheet.create({
    container: {
        width: '100%',
        height: '1.5@vs',
        backgroundColor: '#f5f5f5'
    }
})

export default Divider1;