import React from "react";
import {View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';

const Foregroundcurve = (props) => {

    return(
        <View style={{...localStyles.container, ...props.style}}>
            {props.children}
        </View>
    )
}

const localStyles = ScaledSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
        borderTopStartRadius: "40@ms",
        borderTopEndRadius: "40@ms",
        paddingHorizontal: "15@ms",
        paddingVertical: "20@vs",
    }
});

export default Foregroundcurve;
