import React from 'react';
import {View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/Ionicons';
import Constants from '../common/Constants';

import color from '../resources/color';
import TextView from '../utils/TextView';
import Divider9 from './Divider9';

const TargetItemComponent = (props) => {

    const targetDetailsModal = props.data
    return(
        <View style={localStyles.container}>
            <View style={localStyles.itemContainer}>
                <View style={{...localStyles.roundContainer, backgroundColor: targetDetailsModal.targetAchieved === Constants.targetAchieved ? color.targetAchieved : targetDetailsModal.targetAchieved === Constants.targetNotAchieved ? color.targetNotAchieved : color.light_grey}}>
                    <Icon name={'ios-disc-outline'} color={targetDetailsModal.targetAchieved === Constants.notAvailable ? color.account_options : color.white} size={localStyles.iconSize} />
                </View>
                <View style={localStyles.textContainer}>
                    <TextView style={localStyles.targetCountText} text={`${Constants.target} ${props.index + 1}`} />
                    <TextView style={localStyles.targetText} text={targetDetailsModal.target} />
                </View>
                <TextView style={{...localStyles.targetAchievedText, color: targetDetailsModal.targetAchieved === Constants.targetAchieved ? color.targetAchieved : targetDetailsModal.targetAchieved === Constants.targetNotAchieved ? color.targetNotAchieved : color.black}} text={targetDetailsModal.targetAchieved} />
            </View>
            <Divider9 />
        </View>
    )
}

const localStyles = ScaledSheet.create({
    container: {
        width: '100%',
    },
    itemContainer: {
        flexDirection: 'row',
        margin: "10@ms",
        alignItems: 'center'
    },
    roundContainer: {
        borderRadius: "90@ms", 
        padding: "8@ms", 
        backgroundColor: color.account_options
      },
    iconSize: "20@ms",
    textContainer: {
        flex: 1,
        marginHorizontal: "10@ms"
    },
    targetCountText: {
        fontFamily: "HeliosAntique-Regular",
        fontSize: "13@ms",
        color: color.black

    },
    targetText: {
        fontFamily: 'HeliosAntique-Bold',
        fontSize: "13@ms",
        color: color.black,
        marginTop: "5@ms"
    },
    targetAchievedText: {
        fontFamily: 'HeliosAntique-Bold',
        fontSize: "11@ms"
    }
});

export default TargetItemComponent;