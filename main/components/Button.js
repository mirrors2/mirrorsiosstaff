import React from "react";
import {View, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';

import TextView from "../utils/TextView";

import colors from '../resources/color';

const Button = (props) => {
    return(
        <TouchableOpacity onPress={() => props.onPress()}>
            <View style={{...localStyles.container, ...props.style}}>
                <TextView 
                    style={{...localStyles.textStyle, ...props.textStyle}}
                    text={props.text}/>
            </View>
        </TouchableOpacity>
    )
}

const localStyles = ScaledSheet.create({
    container: {
        backgroundColor: colors.primaryColor,
        borderRadius: "5@ms"
    },
    textStyle: {
        textAlign: 'center',
        fontSize: "15@ms",
        fontFamily: 'HeliosAntique-Bold',
        paddingVertical: "10@ms",
        paddingHorizontal: "8@ms",
        color: colors.white
    }
})

export default Button;