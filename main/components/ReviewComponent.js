import React, {useState} from 'react';
import {View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';

import color from '../resources/color';
import Divider9 from '../components/Divider9';
import ImageView from '../utils/ImageView';
import TextView from '../utils/TextView';
import {Rating} from 'react-native-ratings';

const ReviewComponent = props => {
  const item = props.item;
  const [imageError, setImageError] = useState(false);

  return (
    <View key={props.index}>
      <View style={localStyles.container}>
        <ImageView
          style={localStyles.profileImage}
          resizeMode={'contain'}
          image={
            imageError
              ? require('../resources/drawable/profile.png')
              : {uri: item.image}
          }
          onError={err => setImageError(true)}
        />

        <View style={localStyles.textContainer}>
          <TextView style={localStyles.nameText} text={item.name} />
          <TextView style={localStyles.dateText} text={item.date} />
        </View>

        <Rating
          type="custom"
          imageSize={localStyles.rattngStartSize}
          readonly={true}
          startingValue={item.rating}
          ratingCount={5}
          ratingColor={color.primaryColor}
          ratingBackgroundColor={color.light_grey}
          tintColor='#fff'
        />
      </View>
      <TextView style={localStyles.reviewText} text={item.review} />
      <Divider9 />
    </View>
  );
};

const localStyles = ScaledSheet.create({
  container: {
    width: '100%',
    padding: '10@ms',
    flexDirection: 'row',
  },
  profileImage: {
    width: '35@ms',
    height: '35@ms',
    resizeMode: 'contain',
    borderColor: color.light_grey,
    borderRadius: '100@ms',
    borderWidth: '1.5@ms',
  },
  textContainer: {
    marginHorizontal: '13@ms',
    alignSelf: 'center',
  },
  nameText: {
    fontFamily: 'HeliosAntique-SemiBold',
    fontSize: '12@ms',
    color: color.black,
  },
  dateText: {
    fontFamily: 'Helios Antique',
    fontSize: '11@ms',
    color: color.account_options,
    marginTop: '3@vs',
  },
  reviewText: {
    fontFamily: 'Helios Antique',
    fontSize: '12@ms',
    marginHorizontal: '10@ms',
    marginTop: '3@vs',
    marginBottom: '5@ms',
    color: color.account_options,
  },
  rattngStartSize: '22@ms',
});

export default ReviewComponent;
