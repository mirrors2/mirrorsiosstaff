import React from "react";
import {TouchableOpacity, View} from 'react-native';
import { ScaledSheet } from "react-native-size-matters";

import color from '../resources/color';
import TextView from '../utils/TextView';

const CategoryComponent = (props) => {

    const isSelected = props.selectedCategory === props.data.name;
    return(
        <TouchableOpacity onPress={() => props.onPress()}>
            <View style={localStyles.container}>
                <TextView style={{...localStyles.text, color: isSelected ? color.black : color.lead_disable_gray_color}} text={props.data.name}/>
            </View>
        </TouchableOpacity>
    )
}

const localStyles = ScaledSheet.create({
    container: {
        paddingHorizontal: "12@ms",
        paddingVertical: "5@vs"
    },
    text: {
        fontFamily: 'HeliosAntique-Bold',
        fontSize: "12@ms",
        textTransform: 'uppercase'
    }
    
});

export default CategoryComponent;