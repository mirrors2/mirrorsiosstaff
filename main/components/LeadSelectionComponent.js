import React from "react";
import {TouchableOpacity, View} from 'react-native';
import { ScaledSheet } from "react-native-size-matters";

import color from '../resources/color';
import TextView from '../utils/TextView';

const LeadSelectionComponent = (props) => {

    const isSelected = props.selectedLead === props.text;
    return(
        <TouchableOpacity onPress={() => props.onPress()}>
            <View style={isSelected ? localStyles.container : {...localStyles.container, ...localStyles.disableStyle}}>
                <TextView style={{...localStyles.text, color: isSelected ? color.white : color.black}} text={props.text}/>
            </View>
        </TouchableOpacity>
    )
}

const localStyles = ScaledSheet.create({
    container: {
        backgroundColor: color.primaryColor,
        borderRadius: "5@ms",
        marginRight: "8@ms",
        paddingHorizontal: "8@ms",
        paddingVertical: "5@vs"
    },
    disableStyle: {
        backgroundColor: color.white,
    },
    text: {
        fontFamily: "HeliosAntique-Regular",
        fontSize: "12@ms",
        fontWeight: '500',
        textTransform: 'uppercase'
    }
    
});

export default LeadSelectionComponent;