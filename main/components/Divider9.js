import React from 'react';
import {View} from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';

const Divider9 = (props) => {
    return(<View style={localStyles.container}/>)
}

const localStyles = ScaledSheet.create({
    container: {
        width: '100%',
        height: '9@ms',
        backgroundColor: '#f5f5f5'
    }
})

export default Divider9;