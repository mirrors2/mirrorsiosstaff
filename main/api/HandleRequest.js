import Constants from '../common/Constants';
import {login, dashboardData, 
    leads, leadDetail, startStop, postReview, receivedPayment, downloadInvoice,
    services, myReviews, commission, salary, serviceSale, productSale, target, salaryDetails, targetDetails} from './API';
import ResponseModal from './ResponseModal';

const apiRequest = async ({url, method, headers, payload}) => {
    // console.log('----', url, method, headers, payload);
    let response = {status: "FAILED", message: Constants.unexpectedError};
    try{
        let response = await fetch(url, {
            method: method,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                ...headers
            },
            body: JSON.stringify(payload)
        });
        response = await response.json();
        // console.log(response, '----');
        return response
    }catch (e) {
        return response
    }
}

export const loginRequest = (request) => {
    request.url = login;
    request.method = "POST";
    return new Promise(async (resolve) => {
        try{
            const response = await apiRequest(request);
            resolve(new ResponseModal(response))
        }catch(e){ 
            resolve(new ResponseModal({status: "FAILED", message: Constants.unexpectedError}))
        }
    });
}

export const dashboardDataRequest = (request) => {
    request.url = dashboardData;
    request.method = "POST";
    return new Promise(async (resolve) => {
        try{
            const response = await apiRequest(request);
            resolve(new ResponseModal(response))
        }catch(e){ 
            resolve(new ResponseModal({status: "FAILED", message: Constants.unexpectedError}))
        }
    });
}

export const leadsRequest = (request) => {
    request.url = leads;
    request.method = "POST";
    return new Promise(async (resolve) => {
        try{
            const response = await apiRequest(request);
            resolve(new ResponseModal(response))
        }catch(e){ 
            resolve(new ResponseModal({status: "FAILED", message: Constants.unexpectedError}))
        }
    });
}

export const leadDetailsRequest = (request) => {
    request.url = leadDetail;
    request.method = "POST";
    return new Promise(async (resolve) => {
        try{
            const response = await apiRequest(request);
            resolve(new ResponseModal(response))
        }catch(e){ 
            resolve(new ResponseModal({status: "FAILED", message: Constants.unexpectedError}))
        }
    });
}

export const startStopRequest = (request) => {
    request.url = startStop;
    request.method = "POST";
    return new Promise(async (resolve) => {
        try{
            const response = await apiRequest(request);
            resolve(new ResponseModal(response))
        }catch(e){ 
            resolve(new ResponseModal({status: "FAILED", message: Constants.unexpectedError}))
        }
    });
}


export const postReviewRequest = (request) => {
    request.url = postReview;
    request.method = "POST";
    return new Promise(async (resolve) => {
        try{
            const response = await apiRequest(request);
            resolve(new ResponseModal(response))
        }catch(e){ 
            resolve(new ResponseModal({status: "FAILED", message: Constants.unexpectedError}))
        }
    });
}

export const receivedPaymentRequest = (request) => {
    request.url = receivedPayment;
    request.method = "POST";
    return new Promise(async (resolve) => {
        try{
            const response = await apiRequest(request);
            resolve(new ResponseModal(response))
        }catch(e){ 
            resolve(new ResponseModal({status: "FAILED", message: Constants.unexpectedError}))
        }
    });
}

export const downloadInvoiceRequest = (request) => {
    request.url = downloadInvoice;
    request.method = "POST";
    return new Promise(async (resolve) => {
        try{
            const response = await apiRequest(request);
            resolve(new ResponseModal(response))
        }catch(e){ 
            resolve(new ResponseModal({status: "FAILED", message: Constants.unexpectedError}))
        }
    });
}

export const servicesRequest = (request) => {
    request.url = services;
    request.method = "POST";
    return new Promise(async (resolve) => {
        try{
            const response = await apiRequest(request);
            resolve(new ResponseModal(response))
        }catch(e){ 
            resolve(new ResponseModal({status: "FAILED", message: Constants.unexpectedError}))
        }
    });
}

export const myReviewsRequest = (request) => {
    request.url = myReviews;
    request.method = "POST";
    return new Promise(async (resolve) => {
        try{
            const response = await apiRequest(request);
            resolve(new ResponseModal(response))
        }catch(e){ 
            resolve(new ResponseModal({status: "FAILED", message: Constants.unexpectedError}))
        }
    });
}

export const commissionRequest = (type, request) => {
    request.url = getUrl(type);
    request.method = "POST";
    return new Promise(async (resolve) => {
        try{
            const response = await apiRequest(request);
            resolve(new ResponseModal(response))
        }catch(e){ 
            resolve(new ResponseModal({status: "FAILED", message: Constants.unexpectedError}))
        }
    });
}

export const salaryTargetDetailsRequest = (type, request) => {
    request.url = getUrl(type);
    request.method = "POST";
    return new Promise(async (resolve) => {
        try{
            const response = await apiRequest(request);
            resolve(new ResponseModal(response))
        }catch(e){ 
            resolve(new ResponseModal({status: "FAILED", message: Constants.unexpectedError}))
        }
    });
}
const getUrl = (type) => {
    if(type === Constants.commission){
        return commission;
    }else if(type === Constants.salary){
        return salary;
    }else if(type === Constants.serviceSale){
        return serviceSale;
    }else if(type === Constants.productSale){
        return productSale;
    }else if(type === Constants.target){
        return target;
    }else if(type === Constants.salaryDetails){
        return salaryDetails;
    }else if(type === Constants.targetDetails){
        return targetDetails;
    }
}