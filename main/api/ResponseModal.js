import Constants from "../common/Constants";

export default class ResponseModal {
    
    constructor(response){
        this.status = (response?.status || response?.Status || '')?.toUpperCase();
        this.message = response?.message || Constants.unexpectedError; 
        this.body = response;
    }
}
