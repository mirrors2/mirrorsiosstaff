import {API_URL, API_USER_URL} from "@env"

export const login = `${API_URL}/login.php`;

export const dashboardData = `${API_URL}/gethome.php`;


export const leads = `${API_URL}/leads.php`;

export const leadDetail = `${API_URL}/getleaddetail.php`;

export const startStop = `${API_URL}/startstop.php`;

export const postReview = `${API_URL}/postreview.php`;

export const receivedPayment = `${API_URL}/postpayment.php`;

export const downloadInvoice = `${API_USER_URL}/getinvoice.php`;


export const services = `${API_URL}/getstaffservices.php`;

export const commission = `${API_URL}/getcommission.php`;

export const salary = `${API_URL}/getstaffsalary.php`;

export const serviceSale = `${API_URL}/getsoldservices.php`;

export const productSale = `${API_URL}/getproductsale.php`;

export const target = `${API_URL}/gettargetlist.php`;

export const salaryDetails = `${API_URL}/getsalarydetails.php`;

export const targetDetails = `${API_URL}/gettargetdetail.php`;

export const myReviews = `${API_URL}/getreviewrating.php`;