export default {
    unexpectedError: "Unexpected error occurred, please try again",
    loading: "Loading...",
    notAvailable: "Not available",
    noMoreLeads: "No more Leads",
    noMoreLeadsDec: "Currently you do not have any appointment",
    login: "Login",
    recordsNotFound: 'Records not found',

    dashboard: "Dashboard",
    newLeads: "New Leads",
    ongoingLeads: "Ongoing Leads",
    leadDetails: "Lead Details",
    account: "Account",
    profile: "Profile",
    review_rating: "Review & Rating",
    service: "Service",
    salaryDetails: "Salary Details",
    targetDetails: "Target Details",
    invoiceDetails: "Invoice Details",
    

    dashboardDec: 'You can see your Sales here',
    accountDec: 'You can see your overview',
    bankDec: 'You can see your Bank here',
    reviewDec: 'You can see your Sales here',
    serviceDec: 'You can see your Service here',
    salaryDec: 'You can see your Salary here',

    sales: "Sales",
    assignedCategoryServices: "Assigned category & Services",
    newAppointment: "New Appointment",
    ongoingAppointment: "Ongoing Appointment",
    commission: "Commission",
    salary: "Salary",
    serviceSale: "Service Sale",
    productSale: "Product Sale",
    target: "Target",
    bankDetails: "Bank Details",
    myReviews: "My Reviews",
    logout: "Logout",
    basicDetails: 'Basic Details',
    name: 'Name',
    dob: 'DOB',
    email: 'Email',
    contact: 'Contact',
    designation: 'Designation',
    startDate: 'Start Date',
    endDate: 'end Date',
    location: 'Location',
    primaryLocation: 'Primary Location',
    category: "Category",
    primaryCategory: 'Primary Category',
    secondaryCategory: 'Secondary Category',

    bankName: "Bank Name",
    accountNumber: "Account Number",
    branch: "Branch",
    branchCode: "Branch Code",
    ibanNumber: "IBAN Number",

    targetReached: "Target Reached",
    targetAchieved: "Target Achieved",
    targetNotAchieved: "Target Not Achieved",

    particular: "Particular",
    fine: "Fine",
    advance: "Advance",
    total: "Total",
    salaryStatus: "Salary status",
    amount: "Amount",

    dateAndTime: "Date and Time",
    jobLocation: "Job Location",
    jobDetails: "Job Details",
    reviews: "Review",
    jobCompleted: "Job successfully completed",
    reviewCompleted: "Review added successfully",
    pendingReview: "Pending review",
    write: "Write",
    viewMap: "View map",
    callToCustomer: "Call to customer",
    customerServices: "Customer Services",
    jobStartedAt: "Job Started At",
    jobEndedAt: "Job Ended At",
    paymentType: "Payment Type",
    successfullyReceivedPayment: (type) => `Successfully received payment by ${type}`,
    cash: "Cash",
    card: "Card",
    authCode: "Auth code",
    downloadInvoice: "Download Invoice",
    receivedPayment: "Received Payment",
    quantity: "Qty",
    start: "Start",
    finish: "Finish",
    enter: "Enter",
    alreadyStarted: "Service already started",
    alreadyFinished: "Service already finished",
    pleaseSelectPaymentType: "Please select payment type",
    pleaseEnterAuthCode: "Please enter auth code",
    share: "Share",
    serviceRating: 'Service Rating',
    wouldYouLikeToAddComment: "Would you like to add a comment",
    writeHere: "Write here...",
    send: "Send",
    cancelledByCustomer: "Cancelled by customer",
    areYouSure: "Are you sure ?",
    areYouSureDec: "Are you sure you want to proceed",
    yes: "Yes",
    no: "No",
   
    categories: [
        {id: 2, name: "Hair"}, {id: 3, name: "Facial"}, {id: 4, name: "Eyelash"}, {id: 5, name: "Threading"}, 
        {id: 6, name: "Nail Enhancement"}, {id: 9, name: "Wax"}, {id: 10, name: "Massage"}, {id: 16, name: "Henna Design"},
        {id: 27, name: "Moroccan Bath"}, {id: 28, name: "Makeup"}, {id: 30, name: "Permanent Makeup"}
    ],
    newLeadSelection: ['Today', 'Tomorrow', 'Week', 'Month', 'All'],
    ongoingSelection: ['Today', 'Yesterday', 'Week', 'Month', 'All']
}