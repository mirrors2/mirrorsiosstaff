import Constants from "../common/Constants";

export default class TargetDetailsModal{

    constructor(data, currency){
        let response = data?.details[0];
        const N_A = Constants.notAvailable;
        this.monthyear = response?.monthyear || N_A;
        this.targetreached = `${currency} ${parseFloat(response?.targetreached || 0.00).toFixed(2)}`;
        this.targetpoint = response?.targetpoint || 0;
        this.targets = [...this.getTargets(data?.targets, N_A, currency), ...this.getRetailsTargets(data?.retail_targets, N_A, currency)];
    }

    getTargets(response, N_A, currency){
        return response.map(res => {
            return(
                {
                    target: `${currency} ${parseFloat(res?.target || 0.00).toFixed(2)}`,
                    targetAchieved: res.targetachieved?.toUpperCase() === "Y" ? Constants.targetAchieved : res.targetachieved?.toUpperCase() === "N" ? Constants.targetNotAchieved : N_A
                }
            )
        })
    }

    getRetailsTargets(response, N_A, currency){
        return response.map(res => {
            return(
                {
                    target: `${currency} ${parseFloat(res?.retailtarget || 0.00).toFixed(2)}`,
                    targetAchieved: res?.retailtargetachieved?.toUpperCase() === "Y" ? Constants.targetAchieved : res.retailtargetachieved ?.toUpperCase() === "N" ? Constants.targetNotAchieved : N_A
                }
            )
        })
    }
}