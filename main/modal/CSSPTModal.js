import Constants from "../common/Constants";

export default class CSSPTModal {

    constructor(type, response, currency){
        const N_A = Constants.notAvailable;
        this.monthYear = response?.monthyear || N_A;
        this.totalPrice = `${currency} ${parseFloat(response?.totalprice || response?.commission || 0.00).toFixed(2)}`;
        this.month = response?.month || N_A;
        this.year = response?.year || N_A;
        this.detail = response?.detail || 'N'; //only for salaray -> if 'Y' wii redirect to salary-details screen
        this.targetPoint = response?.targetpoint || '0'; //only for target screen
    }
}