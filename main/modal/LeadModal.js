import { getDate } from "../utils/DateUtil";
import Constants from "../common/Constants";

export default class LeadModal {

    constructor(currentDate, response){
        const N_A = Constants.notAvailable;
        this.refId = response?.ref_id;
        this.name = response?.customer_name || N_A;
        this.serviceName = response?.service_detail_name || N_A;
        this.date = getDate(currentDate, response.date, response.time);
        this.time = response?.time || '';
        this.type = response?.type || 'Unknown';
    }

    
}