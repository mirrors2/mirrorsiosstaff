import Constants from "../common/Constants";

export default class BankModal{

    constructor(response){
        if(response?.status && response.status.toUpperCase() === "FAILED") response = {};

        const N_A = "Not available";
        this.bankName = response?.bank_name || N_A;
        this.accountNumber = response?.account_no || N_A;
        this.branch = response?.branch || N_A;
        this.branchCode = response?.branch_code || N_A;
        this.ibanNumber = response?.IBAN || N_A;
        this.image = response?.image || 'no-image';
    }
}