import Constants from "../common/Constants";

export default class ReviewModal {

    constructor(response){
        const N_A = Constants.notAvailable;
        
        this.star = response?.star || N_A;
        this.review = response?.review || N_A;
    }
}
