import Constants from "../common/Constants";

export default class SalaryDetailsModal{

    constructor(response, currency){
        const N_A = Constants.notAvailable;
        this.monthDate = response?.monthdate || N_A;
        this.totalAmount = `${currency} ${parseFloat(response?.totalamount || 0.00).toFixed(2)}`;
        this.salary = `${currency} ${parseFloat(response?.salary || 0.00).toFixed(2)}`;
        this.fine = `${currency} ${parseFloat(response?.fine || 0.00).toFixed(2)}`;
        this.advance = `${currency} ${parseFloat(response?.advance || 0.00).toFixed(2)}`;
        this.status = response?.status || N_A;
    }
}