import Constants from "../common/Constants";

export default class PaymentModal {

    constructor(response){
        const N_A = Constants.notAvailable;
        
        this.paymentType = response?.paymenttype || N_A;
        this.code = response?.code;
    }
}
