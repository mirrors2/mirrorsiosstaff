import Constants from "../common/Constants";
import ServiceModal from './ServiceModal';
import ReviewModal from './ReviewModal';
import PaymentModal from './PaymentModal';

export default class LeadDetailsModal{

    constructor(response, currency){
        const N_A = Constants.notAvailable;
        
        const details = response?.details?.[0] || {};
        const services = response?.services;
        const review = response?.review;
        const payment = response?.payment;
        
        this.refId = details?.ref_id || N_A;
        this.customerId = details?.customerid || N_A;
        this.customerName = details?.customer_name || N_A;
        this.image = details?.image || N_A;
        this.contact = details?.contact || N_A;
        this.date = details?.date || N_A;
        this.time = details?.time || N_A;
        this.location = details?.location || N_A;
        this.completed = details.completed;
        this.reviewPending = details.reviewpending;
        this.homeService = details?.HomeService || N_A;
        this.customerAddress = details?.CustomerAddress || N_A;
        this.latLng = {latitude: details?.latlng?.split(',')?.[0] || 0.00, longitude: details?.latlng?.split(',')?.[1] || 0.00}
        this.amount = `${currency} ${parseFloat(details?.amount || 0.00).toFixed(2)}`;
        this.jobStartTime = details.jobstarttime && details.jobstarttime != 0 ? details.jobstarttime : undefined;
        this.jobEndTime = details.jobendtime && details.jobendtime != 0 ? details.jobendtime : undefined;
        this.services = this.getServices(services);
        this.review = this.getReview(review);
        this.payment = this.getPayment(payment);
    }

    getServices(response){
        if(response?.length > 0)  return response.map(res => new ServiceModal(res));
    }

    getReview(response) {
        if(response?.length > 0) return new ReviewModal(response?.[0]);
    }

    getPayment(response) {
        if(response?.length > 0) return new PaymentModal(response?.[0]);
    }
}