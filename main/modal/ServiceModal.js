import Constants from "../common/Constants";

export default class ServiceModal {

    constructor(response){
        const N_A = Constants.notAvailable;
        this.serviceName = response?.service_detail_name || N_A;
        this.quantity = response?.quantity || N_A;
        this.cartId = response?.cartid || N_A;
        this.serviceStartTime = response?.service_starttime;
        this.serviceFinishTime = response?.service_finishtime;
    }
}