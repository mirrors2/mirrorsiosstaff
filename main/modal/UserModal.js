import Constants from "../common/Constants";

export default class UserModal{

    constructor(data){
        const response = data?.staff_details;
        const N_A = Constants.notAvailable;

        this.contact = response?.contact || N_A;
        this.dob = response?.dob || N_A;
        this.email = response?.email  || N_A;
        this.endDate = response?.end_date || N_A;
        this.id = response.id;
        this.image = response.image || "no-image";
        this.lastName = response?.last_name || N_A;
        this.location = response?.location || N_A;;
        this.name = response.name || N_A;
        this.permissionUser = response.permission_user;
        this.primaryCategory = response?.primary_category || N_A;
        this.secondaryCategory = response?.secondary_category || N_A;
        this.specialist = response?.specialist || '';
        this.staffCode = response.staff_code;
        this.startDate = response.start_date || N_A;
    }
}