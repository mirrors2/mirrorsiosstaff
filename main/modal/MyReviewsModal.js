import Constants from "../common/Constants";

export default class MyReviewsModal{

    constructor(response){
        const N_A = Constants.notAvailable;
        this.review = response?.review || N_A;
        this.rating = response?.rating || 0;
        this.name = response?.name || N_A;
        this.date = response?.date || N_A;
        this.image = response?.image || '';
    }
}