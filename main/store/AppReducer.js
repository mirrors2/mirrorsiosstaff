const initialState = {
    userData: {},
    bankData: {},
    currency: {currency: "AED"},
    badgeCount: {new: 0, ongoing: 0},
    updateData: false,
}

export const AppReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'userData':
      return Object.assign(state, {userData: action.userData});
    case 'bankData':
      return Object.assign(state, {bankData: action.bankData});
    case 'currency':
        return Object.assign(state, {currency: action.currency});
    case 'badgeCount':
        return Object.assign(state, {badgeCount: action.badgeCount});
    case 'updateData':
      return Object.assign(state, {updateData: action.updateData});
    default:
      return state
  }
}

export const getUserData = (state) => {
  return state?.AppReducer?.userData || {};
}

export const getBankData = (state) => {
  return state?.AppReducer?.bankData || {};
}
 
export const getCurrency = (state) => {
  return state?.AppReducer?.currency?.currency || "AED";
}

export const getBadgeCount = (state) => {
  return state?.AppReducer?.badgeCount || {};
}  