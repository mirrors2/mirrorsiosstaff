import { createStore, combineReducers, applyMiddleware } from 'redux'
import logger from 'redux-logger'

import {AppReducer} from './AppReducer';
const reducer = combineReducers({ AppReducer })

export default createStore(reducer, applyMiddleware(logger))